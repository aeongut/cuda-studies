#include	"cu_def.h"
#include	"cu_global.h"

extern "C" void cu_initialize(cell		**liste,		// SFClist on host
							  int		numleaf,		// Number of leaf cells on host
							  float		*h_bt)			// Boundary values from host

{
///////////////////////////////////////////////////////////////////////////////////////////////////////
//	External variables are exposed 
///////////////////////////////////////////////////////////////////////////////////////////////////////
	extern float*		d_bt;
	extern float*		d_maxgrad;
	extern float*		h_maxgrad;
	extern float*		d_res;
	extern float*		h_res;
	extern float*		h_mesh;
	extern float*		d_mesh;
	extern cudaStream_t stream1;
	extern cudaStream_t stream2;

///////////////////////////////////////////////////////////////////////////////////////////////////////
//	Necessary space on host is allocated. Pointers are used to systematically store data
///////////////////////////////////////////////////////////////////////////////////////////////////////
	int meshSize = 13 * numleaf * sizeof(float);			// size of mesh in bytes
	h_mesh = (float*) malloc(meshSize);						// h_mesh is allocated
	float* center = (float*) (h_mesh);						// center data ocupies 2*numleaf elements
	float* length = (float*) (h_mesh) + 2*numleaf ;			// length data ocupies   numleaf elements
	float* temp   = (float*) (h_mesh) + 3*numleaf ;			// temp   data ocupies   numleaf elements
	float* grad   = (float*) (h_mesh) + 4*numleaf ;			// grad   data ocupies   numleaf elements
	float* SFCng  = (float*) (h_mesh) + 5*numleaf ;			// SFCng  data ocupies 8*numleaf elements

///////////////////////////////////////////////////////////////////////////////////////////////////////
//	Mesh data is moved from original data structure to h_mesh using SFClist(liste in this scope)
///////////////////////////////////////////////////////////////////////////////////////////////////////
	for (int i=0; i<numleaf; i++)
	{
		center[2*i]	  = liste[i]->xcent;
		center[2*i+1] = liste[i]->ycent;
		length[i]	  = liste[i]->length;
		temp[i]		  = liste[i]->temp;
		grad[i]		  = liste[i]->grad;
		for (int j=0; j<8; j++)								// preferred to change int SFCng to float
			SFCng[8*i+j] = (float) liste[i]->SFCng[j];		// it is converted back to int on device
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////
//	Necessary space on device is allocated. h_mesh is then copied to d_mesh
///////////////////////////////////////////////////////////////////////////////////////////////////////	
	cudaMalloc ((void**)&d_mesh, meshSize);
	cudaMemcpy (d_mesh, h_mesh, meshSize, cudaMemcpyHostToDevice);

///////////////////////////////////////////////////////////////////////////////////////////////////////
//	Streams for async memcopy of maximum residual from device to host
///////////////////////////////////////////////////////////////////////////////////////////////////////	
	cudaStreamCreate(&stream1);
	cudaStreamCreate(&stream2);

///////////////////////////////////////////////////////////////////////////////////////////////////////
//	Boundary temperature is sent to device
///////////////////////////////////////////////////////////////////////////////////////////////////////	
	cudaMalloc ((void**)&d_bt, 4*sizeof(float));
	cudaMemcpy (d_bt, h_bt, 4*sizeof(float), cudaMemcpyHostToDevice);

///////////////////////////////////////////////////////////////////////////////////////////////////////
//	Memory allocated on device and host for maximum residual and maximum gradient
///////////////////////////////////////////////////////////////////////////////////////////////////////	
	cudaMalloc		((void**)&d_res, sizeof(float));
	cudaMallocHost	((void**)&h_res, sizeof(int));
	cudaMalloc		((void**)&d_maxgrad, sizeof(float));
	cudaMallocHost	((void**)&h_maxgrad, sizeof(int));
}


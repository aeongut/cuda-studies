float*	d_bt;
float*	d_maxgrad;
float*	h_maxgrad;
float*	d_res;
float*	h_res;

cudaStream_t stream1;
cudaStream_t stream2;

float*	h_center;
float*	d_center;
float*	h_length;
float*	d_length;
float*	h_temp;
float*	d_temp;
float*	h_grad;
float*	d_grad;
int*	h_SFCng;
int*	d_SFCng;

float*	h_mesh;
float*	d_mesh;
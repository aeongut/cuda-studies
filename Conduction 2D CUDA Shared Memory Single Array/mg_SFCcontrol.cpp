#include "meshgen.h"

void meshgen::mg_SFCcontrol(cell *neu)
{
	for(int a=0; a<5; a++)
		for(int b=0; b<5; b++)
			SFCtypes[a][b] = 10*a+b;
	flag[0] = 0;
	flag[1] = 1;

	mg_SFCinitialize(neu);
	mg_SFCcreate( &SFCtypes[0][0], &flag[0], neu);	//calling mg_SFCcreate with case 0 to define the SFClist array
	mg_SFCcreate( &SFCtypes[0][1], &flag[0], neu);	//calling mg_SFCcreate with case 1 starting with root cell
	mg_SFCneighbour();
}
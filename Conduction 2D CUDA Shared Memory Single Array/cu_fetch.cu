#include	"cu_def.h"

extern float* h_mesh;
extern float* d_mesh;

extern "C" void cu_fetch(cell	**liste,		// SFClist on host
						 int	numleaf)
{
	float* d_temp   = (float*) (d_mesh) + 3*numleaf ;		// temp   data ocupies   numleaf elements
	float* d_grad   = (float*) (d_mesh) + 4*numleaf ;		// grad   data ocupies   numleaf elements
	float* h_temp   = (float*) (h_mesh) + 3*numleaf ;		// temp   data ocupies   numleaf elements
	float* h_grad   = (float*) (h_mesh) + 4*numleaf ;		// grad   data ocupies   numleaf elements

	cudaMemcpy (h_temp, d_temp, numleaf*sizeof(float), cudaMemcpyDeviceToHost);
	cudaMemcpy (h_grad, d_grad, numleaf*sizeof(float), cudaMemcpyDeviceToHost);

	for (int i=0; i<numleaf; i++)
	{
		liste[i]->temp	= h_temp[i];
		liste[i]->grad	= h_grad[i];
	}
}

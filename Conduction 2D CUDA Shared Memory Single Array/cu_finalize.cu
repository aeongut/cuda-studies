#include	"cu_def.h"

extern float*	d_bt;
extern float*	d_maxgrad;
extern float*	d_res;
extern float*	d_mesh;

extern float*	h_maxgrad;
extern float*	h_res;
extern float*	h_mesh;

extern "C" void cu_finalize()
{
	cudaFree(d_bt);
	cudaFree(d_res);
	cudaFree(d_maxgrad);
	cudaFree(d_mesh);

	cudaFreeHost(h_res);
	cudaFreeHost(h_maxgrad);

	free(h_mesh);
}


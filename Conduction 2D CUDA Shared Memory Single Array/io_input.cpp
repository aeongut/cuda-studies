#include "meshgen.h"

float meshgen::io_input()
{
in.open("in.dat",ios::in);

float minbt=0.;

	in>>domsize;
	in>>unidiv;
	for(int i=0;i<4;i++)
		in>>bt[i];
	in>>cr;
	minbt=bt[0];
	for(int i=0;i<4;i++)
	{ 
		if(minbt>=bt[i])
		minbt=bt[i];
	}
	numleaf=0;

	return minbt;
}

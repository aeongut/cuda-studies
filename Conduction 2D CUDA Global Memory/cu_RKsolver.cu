#include	"cu_def.h"
#include	"RKsolver.kernel"

extern cell*		d_list;
extern float*		d_bt;
extern float*		d_res;
extern size_t		pitch;
extern cudaStream_t stream1;

extern "C" void cu_RKsolver(int numleaf)
{	
	cudaMemset(d_res, 0, sizeof(float));

	dim3 dimBlock(threadsPerBlock,1,1);
	dim3 dimGrid((numleaf+threadsPerBlock-1)/threadsPerBlock,1);

	RKsolver_kernel<<<dimGrid, dimBlock, 0, stream1>>>(numleaf, d_list, pitch, d_bt, d_res);
//	fprintf(stderr, "%s", cudaGetErrorString (cudaGetLastError ()));
}


#include	"cu_def.h"

extern float* h_maxgrad;
extern float* d_maxgrad;

extern "C" float* cu_getmaxgrad()
{	

	cudaMemcpy(h_maxgrad, d_maxgrad, sizeof(float), cudaMemcpyDeviceToHost);

	return h_maxgrad;
}

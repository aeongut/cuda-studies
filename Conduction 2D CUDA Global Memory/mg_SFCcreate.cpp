/*
This is the code piece to create Paeno-Hilbert type space fillling curve on a 2D domain which is not
occupied by an other closed shape. 
*/

#include "meshgen.h"

void meshgen::mg_SFCcreate(int SFCtype, int SFCflag, cell *neu)
{
	switch(SFCtype)
	{
	case 0:		//start it up
		numleaf = 0;
		io_countcells(neu);
		SFClist = new cell* [numleaf];	//SFClist is an array of pointers pointing to 'cell' type variables
		i = 0;
		break;

	case -1:	//for root cell
		if (SFCflag == 0)
		{
			neu->SFCtype = -1;
			neu->ch[1]->SFCtype = 11;
			mg_SFCcreate(11, 0, neu->ch[1]);
		}
		break;

	case 11:
		if (SFCflag == 0)
		{
			if (neu->ch[0] != 0)
			{
				neu->ch[1]->SFCtype = 21;
				mg_SFCcreate(21, 0, neu->ch[1]);
			}
			else
			{
				neu->SFCorder = i;
				SFClist[i] = neu;
				i++;
				neu->ng[3]->SFCtype = 12;
				mg_SFCcreate(12, 0, neu->ng[3]);
			}
		}
		else
		{
			neu->ng[3]->SFCtype = 12;
			mg_SFCcreate(12, 0, neu->ng[3]);
		}
		break;

	case 12:
		if (SFCflag == 0)
		{
			if (neu->ch[0] != 0)
			{
				neu->ch[1]->SFCtype = 11;
				mg_SFCcreate(11, 0, neu->ch[1]);
			}
			else
			{
				neu->SFCorder = i;
				SFClist[i] = neu;
				i++;
				neu->ng[0]->SFCtype = 13;
				mg_SFCcreate(13, 0, neu->ng[0]);
			}
		}
		else
		{
			neu->ng[0]->SFCtype = 13;
			mg_SFCcreate(13, 0, neu->ng[0]);
		}
		break;

	case 13:
		if (SFCflag == 0)
		{
			if (neu->ch[0] != 0)
			{
				neu->ch[1]->SFCtype = 11;
				mg_SFCcreate(11, 0, neu->ch[1]);
			}
			else
			{
				neu->SFCorder = i;
				SFClist[i] = neu;
				i++;
				neu->ng[1]->SFCtype = 14;
				mg_SFCcreate(14, 0, neu->ng[1]);
			}
		}
		else
		{
			neu->ng[1]->SFCtype = 14;
			mg_SFCcreate(14, 0, neu->ng[1]);
		}
		break;

	case 14:
		if (SFCflag == 0)
		{
			if (neu->ch[0] != 0)
			{
				neu->ch[3]->SFCtype = 41;
				mg_SFCcreate(41, 0, neu->ch[3]);
			}
			else
			{
				neu->SFCorder = i;
				SFClist[i] = neu;
				i++;
				mg_SFCcreate(neu->parent->SFCtype, 1, neu->parent);
			}
		}
		else
			mg_SFCcreate(neu->parent->SFCtype, 1, neu->parent);
		break;

	case 21:
		if (SFCflag == 0)
		{
			if (neu->ch[0] != 0)
			{
				neu->ch[1]->SFCtype = 11;
				mg_SFCcreate(11, 0, neu->ch[1]);
			}
			else
			{
				neu->SFCorder = i;
				SFClist[i] = neu;
				i++;
				neu->ng[0]->SFCtype = 22;
				mg_SFCcreate(22, 0, neu->ng[0]);
			}
		}
		else
		{
			neu->ng[0]->SFCtype = 22;
			mg_SFCcreate(22, 0, neu->ng[0]);
		}
		break;

	case 22:
		if (SFCflag == 0)
		{
			if (neu->ch[0] != 0)
			{
				neu->ch[1]->SFCtype = 21;
				mg_SFCcreate(21, 0, neu->ch[1]);
			}
			else
			{
				neu->SFCorder = i;
				SFClist[i] = neu;
				i++;
				neu->ng[3]->SFCtype = 23;
				mg_SFCcreate(23, 0, neu->ng[3]);
			}
		}
		else
		{
			neu->ng[3]->SFCtype = 23;
			mg_SFCcreate(23, 0, neu->ng[3]);
		}
		break;

	case 23:
		if (SFCflag == 0)
		{
			if (neu->ch[0] != 0)
			{
				neu->ch[1]->SFCtype = 21;
				mg_SFCcreate(21, 0, neu->ch[1]);
			}
			else
			{
				neu->SFCorder = i;
				SFClist[i] = neu;
				i++;
				neu->ng[2]->SFCtype = 24;
				mg_SFCcreate(24, 0, neu->ng[2]);
			}
		}
		else
		{
			neu->ng[2]->SFCtype = 24;
			mg_SFCcreate(24, 0, neu->ng[2]);
		}
		break;

	case 24:
		if (SFCflag == 0)
		{
			if (neu->ch[0] != 0)
			{
				neu->ch[3]->SFCtype = 31;
				mg_SFCcreate(31, 0, neu->ch[3]);
			}
			else
			{
				neu->SFCorder = i;
				SFClist[i] = neu;
				i++;
				mg_SFCcreate(neu->parent->SFCtype, 1, neu->parent);
			}
		}
		else
			mg_SFCcreate(neu->parent->SFCtype, 1, neu->parent);
		break;

	case 31:
		if (SFCflag == 0)
		{
			if (neu->ch[0] != 0)
			{
				neu->ch[3]->SFCtype = 41;
				mg_SFCcreate(41, 0, neu->ch[3]);
			}
			else
			{
				neu->SFCorder = i;
				SFClist[i] = neu;
				i++;
				neu->ng[1]->SFCtype = 32;
				mg_SFCcreate(32, 0, neu->ng[1]);
			}
		}
		else
		{
			neu->ng[1]->SFCtype = 32;
			mg_SFCcreate(32, 0, neu->ng[1]);
		}
		break;

	case 32:
		if (SFCflag == 0)
		{
			if (neu->ch[0] != 0)
			{
				neu->ch[3]->SFCtype = 31;
				mg_SFCcreate(31, 0, neu->ch[3]);
			}
			else
			{
				neu->SFCorder = i;
				SFClist[i] = neu;
				i++;
				neu->ng[2]->SFCtype = 33;
				mg_SFCcreate(33, 0, neu->ng[2]);
			}
		}
		else
		{
			neu->ng[2]->SFCtype = 33;
			mg_SFCcreate(33, 0, neu->ng[2]);
		}
		break;

	case 33:
		if (SFCflag == 0)
		{
			if (neu->ch[0] != 0)
			{
				neu->ch[3]->SFCtype = 31;
				mg_SFCcreate(31, 0, neu->ch[3]);
			}
			else
			{
				neu->SFCorder = i;
				SFClist[i] = neu;
				i++;
				neu->ng[3]->SFCtype = 34;
				mg_SFCcreate(34, 0, neu->ng[3]);
			}
		}
		else
		{
			neu->ng[3]->SFCtype = 34;
			mg_SFCcreate(34, 0, neu->ng[3]);
		}
		break;

	case 34:
		if (SFCflag == 0)
		{
			if (neu->ch[0] != 0)
			{
				neu->ch[1]->SFCtype = 21;
				mg_SFCcreate(21, 0, neu->ch[1]);
			}
			else
			{
				neu->SFCorder = i;
				SFClist[i] = neu;
				i++;
				mg_SFCcreate(neu->parent->SFCtype, 1, neu->parent);
			}
		}
		else
			mg_SFCcreate(neu->parent->SFCtype, 1, neu->parent);
		break;

	case 41:
		if (SFCflag == 0)
		{
			if (neu->ch[0] != 0)
			{
				neu->ch[3]->SFCtype = 31;
				mg_SFCcreate(31, 0, neu->ch[3]);
			}
			else
			{
				neu->SFCorder = i;
				SFClist[i] = neu;
				i++;
				neu->ng[2]->SFCtype = 42;
				mg_SFCcreate(42, 0, neu->ng[2]);
			}
		}
		else
		{
			neu->ng[2]->SFCtype = 42;
			mg_SFCcreate(42, 0, neu->ng[2]);
		}
		break;

	case 42:
		if (SFCflag == 0)
		{
			if (neu->ch[0] != 0)
			{
				neu->ch[3]->SFCtype = 41;
				mg_SFCcreate(41, 0, neu->ch[3]);
			}
			else
			{
				neu->SFCorder = i;
				SFClist[i] = neu;
				i++;
				neu->ng[1]->SFCtype = 43;
				mg_SFCcreate(43, 0, neu->ng[1]);
			}
		}
		else
		{
			neu->ng[1]->SFCtype = 43;
			mg_SFCcreate(43, 0, neu->ng[1]);
		}
		break;

	case 43:
		if (SFCflag == 0)
		{
			if (neu->ch[0] != 0)
			{
				neu->ch[3]->SFCtype = 41;
				mg_SFCcreate(41, 0, neu->ch[3]);
			}
			else
			{
				neu->SFCorder = i;
				SFClist[i] = neu;
				i++;
				neu->ng[0]->SFCtype = 44;
				mg_SFCcreate(44, 0, neu->ng[0]);
			}
		}
		else
		{
			neu->ng[0]->SFCtype = 44;
			mg_SFCcreate(44, 0, neu->ng[0]);
		}
		break;

	case 44:
		if (SFCflag == 0)
		{
			if (neu->ch[0] != 0)
			{
				neu->ch[1]->SFCtype = 11;
				mg_SFCcreate(11, 0, neu->ch[1]);
			}
			else
			{
				neu->SFCorder = i;
				SFClist[i] = neu;
				i++;
				mg_SFCcreate(neu->parent->SFCtype, 1, neu->parent);
			}
		}
		else
			mg_SFCcreate(neu->parent->SFCtype, 1, neu->parent);
		break;
	}

}
#include	"cu_def.h"

extern cell*	d_list;
extern size_t	pitch;

extern "C" void cu_reinitialize(cell	**liste,	// SFClist on host
								int		numleaf)	// Number of cells on host
{

/// cell pointer list SFClist is converted to a cell list
	cell *h_list = (cell*) malloc(numleaf * sizeof(cell));
	for (int i=0; i<numleaf; i++)
		h_list[i] = *liste[i];

/// h_list is copied to device
	cudaMallocPitch ((void**)&d_list, &pitch, sizeof(cell), numleaf);
	cudaMemcpy2D (d_list, pitch, h_list, sizeof(cell), sizeof(cell), numleaf, cudaMemcpyHostToDevice);
}


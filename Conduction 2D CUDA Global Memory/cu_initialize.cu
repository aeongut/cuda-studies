#include	"cu_def.h"
#include	"cu_global.h"

extern "C" void cu_initialize(cell		**liste,		// SFClist on host
							  int		numleaf,		// Number of leaf cells on host
							  float		*h_bt)			// Boundary values from host

{
	extern cell*		d_list;
	extern float*		d_bt;
	extern float*		d_maxgrad;
	extern float*		h_maxgrad;
	extern float*		d_res;
	extern float*		h_res;
	extern size_t		pitch;
	extern cudaStream_t stream1;
	extern cudaStream_t stream2;

/// Streams for async memcopy of maximum residual from device to host
	cudaStreamCreate(&stream1);
	cudaStreamCreate(&stream2);

/// cell pointer list SFClist is converted to a cell list
	cell *h_list = (cell*) malloc(numleaf * sizeof(cell));
	for (int i=0; i<numleaf; i++)
		h_list[i] = *liste[i];

/// h_list is copied to device

	cudaMallocPitch ((void**)&d_list, &pitch, sizeof(cell), numleaf);
	cudaMemcpy2D (d_list, pitch, h_list, sizeof(cell), sizeof(cell), numleaf, cudaMemcpyHostToDevice);

//	Eri�im �ekli:
//	cell *neu = (cell*)((char*)*d_list+0*pitch);
//	printf("%d\n",neu->level);

/// boundary temperature is sent to device
	cudaMalloc ((void**)&d_bt, 4*sizeof(float));
	cudaMemcpy (d_bt, h_bt, 4*sizeof(float), cudaMemcpyHostToDevice);

/// memory allocated on device for residual, maximum gradient, host residual 
	cudaMalloc		((void**)&d_res, sizeof(float));
	cudaMallocHost	((void**)&h_res, sizeof(int));
	cudaMalloc		((void**)&d_maxgrad, sizeof(float));
	cudaMallocHost	((void**)&h_maxgrad, sizeof(int));
}


#include "meshgen.h"

void meshgen::mg_forminit(cell *neu)
{	
	neu->parent  =new cell;
	
	for(int i=0;i<4;i++) neu->ng[i]=new cell;

	for(int i=0;i<4;i++) neu->ch[i]=new cell;

	for(int i=0;i<4;i++)  neu->ng[i]=NULL;

	for(int i=0;i<4;i++)  neu->ch[i]=NULL;

	neu->parent=NULL;

}
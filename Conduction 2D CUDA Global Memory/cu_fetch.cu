#include	"cu_def.h"

extern cell*	d_list;
extern size_t	pitch;

extern "C" void cu_fetch(cell	**liste,		// SFClist on host
						 int	numleaf)
{

	cell *h_list = (cell*) malloc (numleaf * sizeof(cell));
	cudaMemcpy2D (h_list, sizeof(cell), d_list, pitch, sizeof(cell), numleaf, cudaMemcpyDeviceToHost);

//	printf("%f",*liste[1]->temp);

	int a;
	for (a=0; a<numleaf; a++)
	{
	//	*liste[a]->temp = *h_list[a].temp;
	//	*liste[a]->grad = *h_list[a].grad;
		*liste[a] = h_list[a];
	}
}

#include "meshgen.h"

void meshgen::mg_SFCcontrol(cell *neu)
{
	mg_SFCinitialize(neu);
	mg_SFCcreate( 0, 0, neu);	//calling mg_SFCcreate with case 0 to define the SFClist array
	mg_SFCcreate(-1, 0, neu);	//calling mg_SFCcreate with case -1 starting with root cell
	mg_SFCneighbour();
}
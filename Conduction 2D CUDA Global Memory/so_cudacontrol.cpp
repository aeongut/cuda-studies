#include	"meshgen.h"
#include	"cu_def.h"

void meshgen::so_cudacontrol()
{
	int		counter=0;
	float	inres=0;

	cu_initialize(SFClist, numleaf, bt);

	do
	{
		cu_RKsolver(numleaf);
		res = cu_getres();
		if(counter == 0)
		{
			*res = 1.0f;
			inres = *res;
		}
		if(*res <= 0.3f * inres)
		{
			inres = *res;
			cu_gradcalc(numleaf);
			maxgrad = cu_getmaxgrad();
			cu_fetch(SFClist, numleaf);
			so_findrefcell(root);
			so_refine(root);
			mg_SFCcontrol(root);
			cu_reinitialize(SFClist, numleaf);
			cout << counter << '\t' <<"refinement was performed\n";
		}
		if(counter%50==0)
			cout << counter << '\t' << scientific << *res << endl;
		counter = counter+1;
	}
	while(*res>=1E-5);
	cout << counter << '\t' << scientific << *res << endl << "DONE!" << endl;
	cu_fetch(SFClist, numleaf);
	cu_finalize();
}


#ifndef _DATASTRUCT_H_
#define _DATASTRUCT_H_

struct cell 
{
	float		xcent, ycent;
	int			level;
	int			SFCorder, SFCtype, SFCng[8];
	float		length;
	float		temp, grad;
	int			*ref;
	struct cell	*ng[4];	// ng[0]=east neigh, ng[1]=north neigh, ng[2]=west neigh, ng[3]=south neigh
	struct cell	*ch[4];
	struct cell	*parent;
};

#endif


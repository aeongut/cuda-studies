#ifndef _MESHGEN_H_
#define _MESHGEN_H_

//    includes    //////////////////////////////////////////////////////////////
#include	<iostream>
#include	<fstream>
#include	<math.h>
#include	<cstdlib>
#include	<stdlib.h>
#include	<time.h>
#include	"datastruct.h"

using namespace std;

class meshgen : public cell
{
public:

	fstream in, out, resid;

	float	domsize, cr;
	float	bt[4];  // bt[0]=east_bt, bt[1]=north_bt, bt[2]=west_bt bt[3]=south_bt
	float	*res, *maxgrad;
	int		unidiv, numleaf;
	int		i;
	time_t	startTime, endTime;

	cell	**SFClist;
	cell	*root;

	float	io_input();
	void	io_geominfo();
	void	io_countcells(cell *neu);
	void	io_file();
	void	io_tecplot(cell *neu);
	void	io_tecplot2();
	void	io_timer(int);

	void	mg_createroot();
	void	mg_forminit(cell *neu);
	void	mg_createchild(cell *nw);
	void	mg_centercoord(int cellnum, cell *neu);
	void	mg_uniformmesh(cell *neu);
	void	mg_setneighbors(int cellnum,cell *neu);
	void	mg_SFCcontrol(cell *neu);
	void	mg_SFCcreate(int SFCtype, int SFCflag, cell *neu);
	void	mg_SFCinitialize(cell *neu);
	void	mg_SFCneighbour();

	void	so_initcells(float mnbt,cell *neu);
	void	so_solve();
	void	so_unisolve(cell *neu);
	void	so_rk4(cell *nw);
	float	so_Rcalc(cell *neu);
	void	so_gradcalc(cell *neu);
	void	so_findrefcell(cell *neu);
	void	so_refine(cell *neu);
	void	so_cudacontrol();

};

#endif


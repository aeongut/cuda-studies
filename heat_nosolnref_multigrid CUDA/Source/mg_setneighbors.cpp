#include "meshgen.h"

void meshgen::mg_setneighbors(int cellnum, cell *neu)
{
// ne
if(cellnum==0)
{ 
neu->southn=neu->parent->ch[3];
neu->westn=neu->parent->ch[1];

      if(neu->parent->northn!= 0 && neu->parent->northn->ch[0]!= 0)
	  {  neu->northn=neu->parent->northn->ch[3];
	     neu->parent->northn->ch[3]->southn=neu;
	  }
	  if(neu->parent->northn!= 0 && neu->parent->northn->ch[0]== 0)
	  neu->northn=neu->parent->northn;

			if(neu->parent->eastn!= 0 && neu->parent->eastn->ch[0]!= 0)
			{	neu->eastn=neu->parent->eastn->ch[1];
			    neu->parent->eastn->ch[1]->westn=neu;
			}
	        if(neu->parent->eastn!= 0 && neu->parent->eastn->ch[0]== 0)
		    neu->eastn=neu->parent->eastn;
}
// nw
else if(cellnum==1)
{ 
neu->southn=neu->parent->ch[2];
neu->eastn=neu->parent->ch[0];

      if(neu->parent->northn!= 0 && neu->parent->northn->ch[0]!= 0)
	  {  neu->northn=neu->parent->northn->ch[2];
	     neu->parent->northn->ch[2]->southn=neu;
	  }
	  if(neu->parent->northn!= 0 && neu->parent->northn->ch[0]== 0)
	  neu->northn=neu->parent->northn;

			if(neu->parent->westn!= 0 && neu->parent->westn->ch[0]!= 0)
			{	neu->westn=neu->parent->westn->ch[0];
			    neu->parent->westn->ch[0]->eastn=neu;
			}
	        if(neu->parent->westn!= 0 && neu->parent->westn->ch[0]== 0)
		    neu->westn=neu->parent->westn;
}

// sw
 else if(cellnum==2)
{ 
neu->northn=neu->parent->ch[1];
neu->eastn=neu->parent->ch[3];

      if(neu->parent->southn!= 0 && neu->parent->southn->ch[0]!= 0)
	  {  neu->southn=neu->parent->southn->ch[1];
	     neu->parent->southn->ch[1]->northn=neu;
	  }
	  if(neu->parent->southn!= 0 && neu->parent->southn->ch[0]== 0)
	  neu->southn=neu->parent->southn;

			if(neu->parent->westn!= 0 && neu->parent->westn->ch[0]!= 0)
			{	neu->westn=neu->parent->westn->ch[3];
			    neu->parent->westn->ch[3]->eastn=neu;
			}
	        if(neu->parent->westn!= 0 && neu->parent->westn->ch[0]== 0)
		    neu->westn=neu->parent->westn;
}

// se
else if(cellnum==3)
{
neu->northn=neu->parent->ch[0];
neu->westn=neu->parent->ch[2];

      if(neu->parent->southn!= 0 && neu->parent->southn->ch[0]!= 0)
	  {  neu->southn=neu->parent->southn->ch[0];
	     neu->parent->southn->ch[0]->northn=neu;
	  }
	  if(neu->parent->southn!= 0 && neu->parent->southn->ch[0]== 0)
	  neu->southn=neu->parent->southn;

			if(neu->parent->eastn!= 0 && neu->parent->eastn->ch[0]!= 0)
			{	neu->eastn=neu->parent->eastn->ch[2];
			    neu->parent->eastn->ch[2]->westn=neu;
			}
	        if(neu->parent->eastn!= 0 && neu->parent->eastn->ch[0]== 0)
		    neu->eastn=neu->parent->eastn;
}

else
   { cout<<"\n wrong cell number\n\n";
	 exit(EXIT_FAILURE);
   }

if(neu->level>0 && neu->level<unidiv)
for(int i=0;i<4;i++)
mg_setneighbors(i,neu->ch[i]);
}

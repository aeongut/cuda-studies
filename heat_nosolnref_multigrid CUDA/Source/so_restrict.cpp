#include "meshgen.h"

void meshgen::so_restrict(cell *neu, int st)
{	 

	if(neu->level != (unidiv-st))
	{
		for(int i=0;i<4;i++)
		so_restrict(neu->ch[i],st);
	}

if(neu->level == unidiv-st)
{
	neu->resid=0.25f*(neu->ch[0]->resid+neu->ch[1]->resid+neu->ch[2]->resid+neu->ch[3]->resid);
}
}
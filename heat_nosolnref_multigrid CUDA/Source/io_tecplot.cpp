#include "meshgen.h"

void meshgen::io_tecplot(cell *neu)
{
	if(neu->ch[0] != 0)
	{
		for(int i=0;i<4;i++)
		io_tecplot(neu->ch[i]);
	}
	
if(neu->ch[0] == 0)
	{
		out<<(neu->xcent+neu->length/2)<<"	"<<(neu->ycent+neu->length/2)<<"	"<<(neu->temp)<<endl;
		out<<(neu->xcent-neu->length/2)<<"	"<<(neu->ycent+neu->length/2)<<"	"<<(neu->temp)<<endl;
		out<<(neu->xcent-neu->length/2)<<"	"<<(neu->ycent-neu->length/2)<<"	"<<(neu->temp)<<endl;
		out<<(neu->xcent+neu->length/2)<<"	"<<(neu->ycent-neu->length/2)<<"	"<<(neu->temp)<<endl;
	}

}
#include	"cu_def.h"
#include	"calcresid.kernel"

extern float*	d_bt;
extern float*	d_RMS;
extern float*	d_mesh;

extern "C" void cu_calcresid(int* numleaf)
{
	//cudaMemset(d_maxgrad, 0, sizeof(float));

	dim3 dimBlock(threadsPerBlock,1,1);
	dim3 dimGrid((numleaf[0]+threadsPerBlock-1)/threadsPerBlock,1);
	gradcalc_kernel<<<dimGrid, dimBlock>>>(numleaf[0], d_bt, d_RMS, d_mesh);
}


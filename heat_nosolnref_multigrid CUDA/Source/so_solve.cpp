#include "meshgen.h"

void meshgen::so_solve()
{	
	int iterno=0;
	do
	{
		//fine grid iteration part
		for(int j=0;j<iter[0];j++)
		{	rms=0.;
			iterno=iterno+1;
			so_unisolve(root);

			so_calcresid(root);
			totcell=1;
			so_rmscalc(root);
			rms=sqrt(rms/(float(totcell-1)));
			if(iterno%50==0)
			cout<<iterno<<'\t'<<rms<<endl;
			resid<<iterno<<'\t'<<log10(rms)<<endl;
		}


		//restriction part
		for(int st=1;st<step;st++)
		{	so_matrixcoef(root,st);
			so_restrict(root,st);
			for(int gi=0;gi<iter[st];gi++)
				so_gaussseidel(root,st);

			if(st<(step-1))
				so_renewresid(root,st);
		}

		//prolongation part
		for(int st=0;st<(step-1);st++)
		{	int a;
			a=step-st-1;
		
			so_calcerrpr(root,a);

			if(st!=step-2)
			{	so_correcterr(root,a-1);
				for(int gi=0;gi<iter[step+st];gi++)
					so_gaussseidel(root,a-1);
			}
		}

		//correction and final iteration part
		so_correcttemp(root);

		for(int gi=0;gi<iter[2*step-2];gi++)
		{	rms=0.;
			iterno=iterno+1;
			so_unisolve(root);	

			so_calcresid(root);
			totcell=1;
			so_rmscalc(root);
			rms=sqrt(rms/(totcell-1));
			if(iterno%50==0)
			cout<<iterno<<'\t'<<rms<<endl;
			resid<<iterno<<'\t'<<log10(rms)<<endl;
		}

	} while(rms>=1.7E-4);

}

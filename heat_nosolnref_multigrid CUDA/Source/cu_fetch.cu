#include	"cu_def.h"

extern float* h_mesh;
extern float* d_mesh;

extern "C" void cu_fetch(cell	***liste,		// SFClist on host
						 int*	numleaf)
{
	float* d_temp   = (float*) (d_mesh) + 3*numleaf[0] ;
	float* h_temp   = (float*) (h_mesh) + 3*numleaf[0] ;
	//float* d_grad   = (float*) (d_mesh) + 4*numleaf[0] ;
	//float* h_grad   = (float*) (h_mesh) + 4*numleaf[0] ;

	cudaMemcpy (h_temp, d_temp, numleaf[0]*sizeof(float), cudaMemcpyDeviceToHost);
	//cudaMemcpy (h_grad, d_grad, numleaf*sizeof(float), cudaMemcpyDeviceToHost);

	for (int i=0; i<numleaf[0]; i++)
	{
		liste[0][i]->temp	= h_temp[i];
		//liste[0][i]->grad	= h_grad[i];
	}
}

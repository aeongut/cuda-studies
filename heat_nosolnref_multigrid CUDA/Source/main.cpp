#include "meshgen.h"

int main()
{
	meshgen gr;

	gr.io_input();

	gr.mg_createroot();
	gr.mg_SFCcontrol(gr.root);

	gr.so_cudacontrol();

//	gr.so_solve();

	gr.io_file();
	gr.io_tecplot(gr.root);
	gr.io_tecplot2();

	cout <<endl<<"End of program."<<endl;
	getchar();
	return 0;
}
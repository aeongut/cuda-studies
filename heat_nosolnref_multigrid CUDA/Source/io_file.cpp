#include "meshgen.h"

void meshgen::io_file()
{
out.open("tec.dat",ios::out);
out<<"Title: \"2-D Heat Conduction Problem Solver with using Cartesian Grid\" \n";
out<<"variables=x,y,T \n";
out<<"zone t=\"multigrid_noref\" i="<<numleaf[0]*4<<", j="<<numleaf[0]<<", f=fepoint et=quadrilateral \n";

}

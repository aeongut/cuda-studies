#ifndef MESHGEN
#define MESHGEN

#include<iostream>
using namespace std;
#include<fstream>
#include<math.h>
#include<cstdlib>
#include<stdlib.h>
#include "datastruct.h"

class meshgen : public cell
{
	public:

		fstream in, out, resid;

		float	domsize;
		float	bt[4];  // bt[0]=east_bt, bt[1]=west_bt, bt[2]=north_bt bt[3]=south_bt
		float	maxgrad, rms;
		int		unidiv, *numleaf, totcell, maho, mahop;
		int		step, *iter;
		int		i, SFCtypes[5][5], flag[2];

		cell	***SFClist;
		cell	*root;

		void	io_input();
		void	io_countcells(cell*, int);
		void	io_file();
		void	io_tecplot(cell *neu);
		void	io_tecplot2();

		void	mg_createroot();
		void	mg_forminit(cell *neu);
		void	mg_createchild(cell *nw);
		void	mg_centercoord(int cellnum, cell *neu);
		void	mg_uniformmesh(cell *neu);
		void	mg_setneighbors(int cellnum,cell *neu);
		void	mg_SFCcontrol(cell *neu);
		void	mg_SFCcreate(int*, int*, int, cell*);
		void	mg_SFCinitialize(cell *neu);
		void	mg_SFCneighbour(int);

		void	so_solve();
		void	so_unisolve(cell *neu);
		void	so_rk4(cell *nw); 
		float	so_Rcalc(cell *neu);
		void	so_calcresid(cell *neu);
		void	so_restrict(cell *neu, int st);
		void	so_matrixcoef(cell *neu, int st);
		void	so_gaussseidel(cell *neu, int st);
		void	so_renewresid(cell *neu, int st);
		void	so_calcerrpr(cell *neu, int st);
		void	so_correcterr(cell *neu, int st);
		void	so_correcttemp(cell *neu);
		void	so_rmscalc(cell *neu);
		void	so_cudacontrol();
};

#endif
#include "meshgen.h"

void meshgen::mg_SFCinitialize(cell *neu)
{
///////////////////////////////////////////////////////////////////////////////////////////////////////
//	SFCorder is used to store the position of a cell on SFClist. For multigrid case a cell can be a 
//	part of different grids. Therefore for each cell SFCorder[step] elements are necessary for storing
//	the order of each element on SFClist belonging to different grids.
//	SFCtype is needed in SFC_create function.
///////////////////////////////////////////////////////////////////////////////////////////////////////
	if(neu->ch[0] != 0)
	{
		if (neu->level > unidiv-step)
		{
			neu->SFCorder = new int[step];
			for (int z = 0; z<step; z++)
				neu->SFCorder[z] = -1;
		}
		neu->SFCtype	= new int(0);
		for(int i=0;i<4;i++)
			mg_SFCinitialize(neu->ch[i]);
	}
	else
	{
		if (neu->level > unidiv-step)
		{
			neu->SFCorder = new int[step];
			for (int z = 0; z<step; z++)
				neu->SFCorder[z] = -1;
		}
		neu->SFCtype	= new int(0);
	}
}

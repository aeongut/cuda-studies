#include "meshgen.h"

void meshgen::so_renewresid(cell *neu, int st)
{	
	if(neu->level != (unidiv-st))
	{
		for(int i=0;i<4;i++)
		so_renewresid(neu->ch[i],st);
	}

if(neu->level == unidiv-st)
{	float t1=0.0f, t2=0.0f, t3=0.0f, t4=0.0f;

	if(neu->eastn != 0)
		t1=(*neu->ae)*neu->eastn->err;
	if(neu->westn != 0)
		t2=(*neu->aw)*neu->westn->err;
	if(neu->northn != 0)
		t3=(*neu->an)*neu->northn->err;
	if(neu->southn != 0)
		t4=(*neu->as)*neu->southn->err;
	neu->resid=neu->resid+t1+t2+t3+t4-(*neu->ap)*neu->err;


}
}
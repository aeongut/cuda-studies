#include	"meshgen.h"
#include	"cu_def.h"

void meshgen::so_cudacontrol()
{

	cu_initialize(SFClist, numleaf, step, bt);
	for(int s=0; s<10;s++)
	{
		cu_RKsolver(numleaf);
		cout << s << endl;
	}
	cu_fetch(SFClist, numleaf);

	//cu_calcresid(numleaf);
}

#include "meshgen.h"

void meshgen::mg_SFCcontrol(cell *neu)
{
///////////////////////////////////////////////////////////////////////////////////////////////////////
//	SFCtypes[][] and flag[] are used in SFC_create function
///////////////////////////////////////////////////////////////////////////////////////////////////////
	for(int a=0; a<5; a++)
		for(int b=0; b<5; b++)
			SFCtypes[a][b] = 10*a+b;
	flag[0] = 0;
	flag[1] = 1;
			
	for(int set=0; set<step; set++)
		numleaf[set] = 0;

///////////////////////////////////////////////////////////////////////////////////////////////////////
//	Now we need more than one SFC lists. SFClist is a cell*** type variable. SFClist[][] corresponds to
//	cell* type elements, first index showing the number of the list and second index showing the number
//	of element that is pointed.
///////////////////////////////////////////////////////////////////////////////////////////////////////
	SFClist = new cell** [step];			// first dimension is set as the multigrid step 
	mg_SFCinitialize(neu);					// this function initializes SFCorder and SFCtype vars
	for(int check=0; check<step; check++)	// for each grid an SFClist is created
	{
		mg_SFCcreate( &SFCtypes[0][0], &flag[0], check, neu);	//calling with case 0 to define the SFClist array
		mg_SFCcreate( &SFCtypes[0][1], &flag[0], check, neu);	//calling with case 1 starting with root cell
		mg_SFCneighbour(check);									//finds neigbours in SFCorder format
	}
}
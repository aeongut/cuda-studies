/*
This is the code piece to create Paeno-Hilbert type space fillling curve on a 2D domain which is not
occupied by an other closed shape. 
*/

#include "meshgen.h"

void meshgen::mg_SFCcreate(int* SFCtype, int* SFCflag, int st, cell *neu)
{
	if(*SFCtype == 0)	//start it up
	{

		io_countcells(neu, st);
		SFClist[st] = new cell* [numleaf[st]];	//SFClist is an array of pointers pointing to 'cell' type variables
		i = 0;
	}


	if(*SFCtype == 1)	//for root cell
	{
		if (*SFCflag == 0)
		{
			*neu->SFCtype = 1;
			*neu->ch[1]->SFCtype = SFCtypes[1][1];
			mg_SFCcreate(&SFCtypes[1][1], &flag[0], st, neu->ch[1]);
		}
	}

	if(*SFCtype == 11)
	{
		if (*SFCflag == 0)
		{
			if (neu->level < (unidiv-st))
			{
				*neu->ch[1]->SFCtype = SFCtypes[2][1];
				mg_SFCcreate(&SFCtypes[2][1], &flag[0], st, neu->ch[1]);
			}
			else if (neu->level == (unidiv-st))
			{
				neu->SFCorder[st] = i;
				SFClist[st][i] = neu;
				i++;
				*neu->southn->SFCtype = SFCtypes[1][2];
				mg_SFCcreate(&SFCtypes[1][2], &flag[0], st, neu->southn);
			}
		}
		else
		{
			*neu->southn->SFCtype = SFCtypes[1][2];
			mg_SFCcreate(&SFCtypes[1][2], &flag[0], st, neu->southn);
		}
	}

	if(*SFCtype == 12)
	{
		if (*SFCflag == 0)
		{
			if (neu->level < (unidiv-st))
			{
				*neu->ch[1]->SFCtype = SFCtypes[1][1];
				mg_SFCcreate(&SFCtypes[1][1], &flag[0], st, neu->ch[1]);
			}
			else if (neu->level == (unidiv-st))
			{
				neu->SFCorder[st] = i;
				SFClist[st][i] = neu;
				i++;
				*neu->eastn->SFCtype = SFCtypes[1][3];
				mg_SFCcreate(&SFCtypes[1][3], &flag[0], st, neu->eastn);
			}
		}
		else
		{
			*neu->eastn->SFCtype = SFCtypes[1][3];
			mg_SFCcreate(&SFCtypes[1][3], &flag[0], st, neu->eastn);
		}
	}

	if(*SFCtype == 13)
	{
		if (*SFCflag == 0)
		{
			if (neu->level < (unidiv-st))
			{
				*neu->ch[1]->SFCtype = SFCtypes[1][1];
				mg_SFCcreate(&SFCtypes[1][1], &flag[0], st, neu->ch[1]);
			}
			else if (neu->level == (unidiv-st))
			{
				neu->SFCorder[st] = i;
				SFClist[st][i] = neu;
				i++;
				*neu->northn->SFCtype = SFCtypes[1][4];
				mg_SFCcreate(&SFCtypes[1][4], &flag[0], st, neu->northn);
			}
		}
		else
		{
			*neu->northn->SFCtype = SFCtypes[1][4];
			mg_SFCcreate(&SFCtypes[1][4], &flag[0], st, neu->northn);
		}
	}

	if(*SFCtype == 14)
	{
		if (*SFCflag == 0)
		{
			if (neu->level < (unidiv-st))
			{
				*neu->ch[3]->SFCtype = SFCtypes[4][1];
				mg_SFCcreate(&SFCtypes[4][1], &flag[0], st, neu->ch[3]);
			}
			else if (neu->level == (unidiv-st))
			{
				neu->SFCorder[st] = i;
				SFClist[st][i] = neu;
				i++;
				mg_SFCcreate(neu->parent->SFCtype, &flag[1], st, neu->parent);
			}
		}
		else
			mg_SFCcreate(neu->parent->SFCtype, &flag[1], st, neu->parent);
	}

	if(*SFCtype == 21)
	{
		if (*SFCflag == 0)
		{
			if (neu->level < (unidiv-st))
			{
				*neu->ch[1]->SFCtype = SFCtypes[1][1];
				mg_SFCcreate(&SFCtypes[1][1], &flag[0], st, neu->ch[1]);
			}
			else if (neu->level == (unidiv-st))
			{
				neu->SFCorder[st] = i;
				SFClist[st][i] = neu;
				i++;
				*neu->eastn->SFCtype = SFCtypes[2][2];
				mg_SFCcreate(&SFCtypes[2][2], &flag[0], st, neu->eastn);
			}
		}
		else
		{
			*neu->eastn->SFCtype = SFCtypes[2][2];
			mg_SFCcreate(&SFCtypes[2][2], &flag[0], st, neu->eastn);
		}
	}

	if(*SFCtype == 22)
	{
		if (*SFCflag == 0)
		{
			if (neu->level < (unidiv-st))
			{
				*neu->ch[1]->SFCtype = SFCtypes[2][1];
				mg_SFCcreate(&SFCtypes[2][1], &flag[0], st, neu->ch[1]);
			}
			else if (neu->level == (unidiv-st))
			{
				neu->SFCorder[st] = i;
				SFClist[st][i] = neu;
				i++;
				*neu->southn->SFCtype = SFCtypes[2][3];
				mg_SFCcreate(&SFCtypes[2][3], &flag[0], st, neu->southn);
			}
		}
		else
		{
			*neu->southn->SFCtype = SFCtypes[2][3];
			mg_SFCcreate(&SFCtypes[2][3], &flag[0], st, neu->southn);
		}
	}

	if(*SFCtype == 23)
	{
		if (*SFCflag == 0)
		{
			if (neu->level < (unidiv-st))
			{
				*neu->ch[1]->SFCtype = SFCtypes[2][1];
				mg_SFCcreate(&SFCtypes[2][1], &flag[0], st, neu->ch[1]);
			}
			else if (neu->level == (unidiv-st))
			{
				neu->SFCorder[st] = i;
				SFClist[st][i] = neu;
				i++;
				*neu->westn->SFCtype = SFCtypes[2][4];
				mg_SFCcreate(&SFCtypes[2][4], &flag[0], st, neu->westn);
			}
		}
		else
		{
			*neu->westn->SFCtype = SFCtypes[2][4];
			mg_SFCcreate(&SFCtypes[2][4], &flag[0], st, neu->westn);
		}
	}

	if(*SFCtype == 24)
	{
		if (*SFCflag == 0)
		{
			if (neu->level < (unidiv-st))
			{
				*neu->ch[3]->SFCtype = SFCtypes[3][1];
				mg_SFCcreate(&SFCtypes[3][1], &flag[0], st, neu->ch[3]);
			}
			else if (neu->level == (unidiv-st))
			{
				neu->SFCorder[st] = i;
				SFClist[st][i] = neu;
				i++;
				mg_SFCcreate(neu->parent->SFCtype, &flag[1], st, neu->parent);
			}
		}
		else
			mg_SFCcreate(neu->parent->SFCtype, &flag[1], st, neu->parent);
	}

	if(*SFCtype == 31)
	{
		if (*SFCflag == 0)
		{
			if (neu->level < (unidiv-st))
			{
				*neu->ch[3]->SFCtype = SFCtypes[4][1];
				mg_SFCcreate(&SFCtypes[4][1], &flag[0], st, neu->ch[3]);
			}
			else if (neu->level == (unidiv-st))
			{
				neu->SFCorder[st] = i;
				SFClist[st][i] = neu;
				i++;
				*neu->northn->SFCtype = SFCtypes[3][2];
				mg_SFCcreate(&SFCtypes[3][2], &flag[0], st, neu->northn);
			}
		}
		else
		{
			*neu->northn->SFCtype = SFCtypes[3][2];
			mg_SFCcreate(&SFCtypes[3][2], &flag[0], st, neu->northn);
		}
	}

	if(*SFCtype == 32)
	{
		if (*SFCflag == 0)
		{
			if (neu->level < (unidiv-st))
			{
				*neu->ch[3]->SFCtype = SFCtypes[3][1];
				mg_SFCcreate(&SFCtypes[3][1], &flag[0], st, neu->ch[3]);
			}
			else if (neu->level == (unidiv-st))
			{
				neu->SFCorder[st] = i;
				SFClist[st][i] = neu;
				i++;
				*neu->westn->SFCtype = SFCtypes[3][3];
				mg_SFCcreate(&SFCtypes[3][3], &flag[0], st, neu->westn);
			}
		}
		else
		{
			*neu->westn->SFCtype = SFCtypes[3][3];
			mg_SFCcreate(&SFCtypes[3][3], &flag[0], st, neu->westn);
		}
	}

	if(*SFCtype == 33)
	{
		if (*SFCflag == 0)
		{
			if (neu->level < (unidiv-st))
			{
				*neu->ch[3]->SFCtype = SFCtypes[3][1];
				mg_SFCcreate(&SFCtypes[3][1], &flag[0], st, neu->ch[3]);
			}
			else if (neu->level == (unidiv-st))
			{
				neu->SFCorder[st] = i;
				SFClist[st][i] = neu;
				i++;
				*neu->southn->SFCtype = SFCtypes[3][4];
				mg_SFCcreate(&SFCtypes[3][4], &flag[0], st, neu->southn);
			}
		}
		else
		{
			*neu->southn->SFCtype = SFCtypes[3][4];
			mg_SFCcreate(&SFCtypes[3][4], &flag[0], st, neu->southn);
		}
	}

	if(*SFCtype == 34)
	{
		if (*SFCflag == 0)
		{
			if (neu->level < (unidiv-st))
			{
				*neu->ch[1]->SFCtype = SFCtypes[2][1];
				mg_SFCcreate(&SFCtypes[2][1], &flag[0], st, neu->ch[1]);
			}
			else if (neu->level == (unidiv-st))
			{
				neu->SFCorder[st] = i;
				SFClist[st][i] = neu;
				i++;
				mg_SFCcreate(neu->parent->SFCtype, &flag[1], st, neu->parent);
			}
		}
		else
			mg_SFCcreate(neu->parent->SFCtype, &flag[1], st, neu->parent);
	}

	if(*SFCtype == 41)
	{
		if (*SFCflag == 0)
		{
			if (neu->level < (unidiv-st))
			{
				*neu->ch[3]->SFCtype = SFCtypes[3][1];
				mg_SFCcreate(&SFCtypes[3][1], &flag[0], st, neu->ch[3]);
			}
			else if (neu->level == (unidiv-st))
			{
				neu->SFCorder[st] = i;
				SFClist[st][i] = neu;
				i++;
				*neu->westn->SFCtype = SFCtypes[4][2];
				mg_SFCcreate(&SFCtypes[4][2], &flag[0], st, neu->westn);
			}
		}
		else
		{
			*neu->westn->SFCtype = SFCtypes[4][2];
			mg_SFCcreate(&SFCtypes[4][2], &flag[0], st, neu->westn);
		}
	}

	if(*SFCtype == 42)
	{
		if (*SFCflag == 0)
		{
			if (neu->level < (unidiv-st))
			{
				*neu->ch[3]->SFCtype = SFCtypes[4][1];
				mg_SFCcreate(&SFCtypes[4][1], &flag[0], st, neu->ch[3]);
			}
			else if (neu->level == (unidiv-st))
			{
				neu->SFCorder[st] = i;
				SFClist[st][i] = neu;
				i++;
				*neu->northn->SFCtype = SFCtypes[4][3];
				mg_SFCcreate(&SFCtypes[4][3], &flag[0], st, neu->northn);
			}
		}
		else
		{
			*neu->northn->SFCtype = SFCtypes[4][3];
			mg_SFCcreate(&SFCtypes[4][3], &flag[0], st, neu->northn);
		}
	}

	if(*SFCtype == 43)
	{
		if (*SFCflag == 0)
		{
			if (neu->level < (unidiv-st))
			{
				*neu->ch[3]->SFCtype = SFCtypes[4][1];
				mg_SFCcreate(&SFCtypes[4][1], &flag[0], st, neu->ch[3]);
			}
			else if (neu->level == (unidiv-st))
			{
				neu->SFCorder[st] = i;
				SFClist[st][i] = neu;
				i++;
				*neu->eastn->SFCtype = SFCtypes[4][4];
				mg_SFCcreate(&SFCtypes[4][4], &flag[0], st, neu->eastn);
			}
		}
		else
		{
			*neu->eastn->SFCtype = SFCtypes[4][4];
			mg_SFCcreate(&SFCtypes[4][4], &flag[0], st, neu->eastn);
		}
	}

	if(*SFCtype == 44)
	{
		if (*SFCflag == 0)
		{
			if (neu->level < (unidiv-st))
			{
				*neu->ch[1]->SFCtype = SFCtypes[1][1];
				mg_SFCcreate(&SFCtypes[1][1], &flag[0], st, neu->ch[1]);
			}
			else if (neu->level == (unidiv-st))
			{
				neu->SFCorder[st] = i;
				SFClist[st][i] = neu;
				i++;
				mg_SFCcreate(neu->parent->SFCtype, &flag[1], st, neu->parent);
			}
		}
		else
			mg_SFCcreate(neu->parent->SFCtype, &flag[1], st, neu->parent);
	}

}
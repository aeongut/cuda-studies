#include "meshgen.h"

void meshgen::io_countcells(cell *neu, int st)
{
	if(neu->level != (unidiv-st))
	{
		for(int i=0;i<4;i++)
			io_countcells(neu->ch[i], st);
	}

	if(neu->level == (unidiv-st))
		numleaf[st]=numleaf[st]+1;

}
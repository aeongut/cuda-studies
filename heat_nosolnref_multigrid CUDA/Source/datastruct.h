#ifndef DATASTRUCT
#define DATASTRUCT

struct cell 
{	
	int		level;
	int		*SFCorder,  SFCng[4], *SFCtype;
	float	xcent, ycent;
	float	length;
	float	temp, grad, resid, err, errpr;
	float	*ae, *aw, *an, *as, *ap;

	cell	*eastn, *westn, *northn, *southn;
	cell	*ch[4];
	cell	*parent;
};

#endif
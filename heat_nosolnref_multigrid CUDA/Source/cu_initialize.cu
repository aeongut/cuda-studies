#include	"cu_def.h"
#include	"cu_global.h"

extern "C" void cu_initialize(cell		***list,			// SFClist on host
							  int		*numleaf,		// Number of leaf cells on host
							  int		step,			// Number of multigrid steps
							  float		*h_bt)			// Boundary values from host

{
///////////////////////////////////////////////////////////////////////////////////////////////////////
//	External variables are exposed 
///////////////////////////////////////////////////////////////////////////////////////////////////////
	extern float*		d_bt;
	extern float*		d_RMS;
	extern float*		h_mesh;
	extern float*		d_mesh;
	extern cudaStream_t stream1;
	extern cudaStream_t stream2;

///////////////////////////////////////////////////////////////////////////////////////////////////////
//	Our purpose is to store all mesh data in a single array and copy this large array to device at once.
//	To systemically reach data on this 1D array, strides for each mesh is determined at very first step.
///////////////////////////////////////////////////////////////////////////////////////////////////////
	int *meshStride = (int*) malloc(step*sizeof(int));		// stride for each mesh
	int totalSize=0;										// total size of mesh 
	int nvar = 9;											// number of variables for each cell
	for (int y=0; y<step; y++)
	{
		if(y==0) meshStride[y] = 0;									// first stride is zero						
		else meshStride[y] = meshStride[y-1] + nvar * numleaf[y-1];	// other strides
		totalSize = totalSize + nvar * numleaf[y];					// total number of cells of all meash
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////
//	For each multigrid mesh we need to copy necessary members of cells like center, length, temp, etc.
//	To reach these members in a coalesced way later when reading from device memory to shared memory,
//	these elements must be grouped. For each mesh, members grouped on 1D array are like below:
//	
//	|			first mesh						|	second mesh		|	...
//	|centercenter..lengthlength...temptemp...	|		...			|	...	
//	
///////////////////////////////////////////////////////////////////////////////////////////////////////
	h_mesh = (float*) malloc(totalSize * sizeof(float));	// h_mesh is allocated

	float** center;	//pointer to find the beginning address of centers (in xy order)
	float** length;	//pointer to find the beginning address of lengths 
	float** temp;	//pointer to find the beginning address of temps 
	float** grad;	//pointer to find the beginning address of grads 
	float** SFCng;	//pointer to find the beginning address of SFCngs (4 neigbours continously)

	center = (float**) malloc(step * sizeof(float));
	length = (float**) malloc(step * sizeof(float));
	temp   = (float**) malloc(step * sizeof(float));
	grad   = (float**) malloc(step * sizeof(float));
	SFCng  = (float**) malloc(step * sizeof(float));

	for(int pt=0; pt<step; pt++)
	{
		center[pt] = (float*)h_mesh + meshStride[pt];					// center data ocupies 2*numleaf elements
		length[pt] = (float*)h_mesh + meshStride[pt] + 2*numleaf[pt];	// length data ocupies   numleaf elements
		temp[pt]   = (float*)h_mesh + meshStride[pt] + 3*numleaf[pt];	// temp   data ocupies   numleaf elements
		grad[pt]   = (float*)h_mesh + meshStride[pt] + 4*numleaf[pt];	// grad   data ocupies   numleaf elements
		SFCng[pt]  = (float*)h_mesh + meshStride[pt] + 5*numleaf[pt];	// SFCng  data ocupies 4*numleaf elements
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////
//	Mesh data is COPIED from original data structure to h_mesh using SFClist(list in this scope) with 
//	the help of pointers created in previous step.
///////////////////////////////////////////////////////////////////////////////////////////////////////
	for (int i=0; i<step; i++)
		for(int j=0; j<numleaf[i]; j++)
		{
			center[i][2*j]	  = list[i][j]->xcent;
			center[i][2*j+1]  = list[i][j]->ycent;
			length[i][j]	  = list[i][j]->length;
			temp[i][j]		  = list[i][j]->temp;
			grad[i][j]		  = list[i][j]->grad;
			for (int k=0; k<4; k++)								// preferred to change int SFCng to float
				SFCng[i][4*j+k] = (float) list[i][j]->SFCng[k];	// it is converted back to int on device
		}

///////////////////////////////////////////////////////////////////////////////////////////////////////
//	Necessary space on device is allocated. h_mesh is then copied to d_mesh
///////////////////////////////////////////////////////////////////////////////////////////////////////	
	cudaMalloc ((void**)&d_mesh, totalSize * sizeof(float));
	cudaMemcpy (d_mesh, h_mesh, totalSize * sizeof(float), cudaMemcpyHostToDevice);

///////////////////////////////////////////////////////////////////////////////////////////////////////
//	Streams for async memcopy of maximum residual from device to host
///////////////////////////////////////////////////////////////////////////////////////////////////////	
	cudaStreamCreate(&stream1);
	cudaStreamCreate(&stream2);

///////////////////////////////////////////////////////////////////////////////////////////////////////
//	Boundary temperature is sent to device
///////////////////////////////////////////////////////////////////////////////////////////////////////	
	cudaMalloc ((void**)&d_bt, 4*sizeof(float));
	cudaMemcpy (d_bt, h_bt, 4*sizeof(float), cudaMemcpyHostToDevice);

///////////////////////////////////////////////////////////////////////////////////////////////////////
//	Memory allocated on device and host for maximum residual and maximum gradient
///////////////////////////////////////////////////////////////////////////////////////////////////////	
	cudaMalloc		((void**)&d_RMS, sizeof(float));
	//cudaMallocHost	((void**)&h_res, sizeof(int));
	//cudaMalloc		((void**)&d_maxgrad, sizeof(float));
	//cudaMallocHost	((void**)&h_maxgrad, sizeof(int));
}


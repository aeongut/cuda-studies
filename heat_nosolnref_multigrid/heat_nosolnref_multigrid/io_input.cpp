#include "meshgen.h"

void meshgen::io_input()
{
in.open("in.dat",ios::in);
resid.open("res.dat",ios::out);
resid<<"variables=iter_num,rms\n";
resid<<"zone t=\"multigrid_noref\" \n";

float minbt=0.;

in>>domsize;
in>>unidiv;
	for(int i=0;i<4;i++)
	in>>bt[i];
in>>step;
iter=new int[2*step-1];
for(int j=0;j<(2*step-1);j++)
	in>>iter[j];

numleaf=0;
maho=1;

}

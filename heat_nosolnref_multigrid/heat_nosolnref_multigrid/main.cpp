#include "meshgen.h"

int main()
{
	meshgen gr;

	gr.io_input();

	gr.mg_createroot();

	gr.so_solve();

	gr.io_countcells(gr.root);
	gr.io_file();
	gr.io_tecplot(gr.root);
	gr.io_tecplot2();


	return 0;
}
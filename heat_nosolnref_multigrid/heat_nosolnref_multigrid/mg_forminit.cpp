#include "meshgen.h"

void meshgen::mg_forminit(cell *neu)
{	
	neu->parent  =new cell;
	neu->eastn   =new cell;
	neu->westn   =new cell;
	neu->northn  =new cell;
	neu->southn  =new cell;
	neu->temp	 =100.;
	neu->grad	 =0.;
	neu->resid	 =0.;
	neu->err	 =0.;

	for(int i=0;i<4;i++) neu->ch[i]=new cell;

	for(int i=0;i<4;i++)  neu->ch[i]=NULL;

neu->parent=NULL;
neu->eastn=NULL;
neu->westn=NULL;
neu->northn=NULL;
neu->southn=NULL;
}

#ifndef DATASTRUCT
#define DATASTRUCT

class cell 
{	
public:

	float xcent, ycent;
	int level;
	float length;
	float temp, grad, resid, err, errpr;
	float *ae, *aw, *an, *as, *ap;

	cell *eastn, *westn, *northn, *southn;
	cell *ch[4];
	cell *parent;
};

#endif
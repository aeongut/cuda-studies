#include "meshgen.h"

void meshgen::so_calcerrpr(cell *neu, int st)
{	
	if(neu->level != (unidiv-st))
	{
		for(int i=0;i<4;i++)
		so_calcerrpr(neu->ch[i],st);
	}

if(neu->level == unidiv-st)
{	
//all edges have neighbors	
	if(neu->eastn != 0 && neu->westn != 0 && neu->northn != 0 && neu->southn != 0)
	{	neu->ch[0]->errpr=0.1875f*neu->eastn->err+0.1875f*neu->northn->err+0.0625f*neu->eastn->northn->err+0.5625f*neu->err;
		neu->ch[1]->errpr=0.1875f*neu->westn->err+0.1875f*neu->northn->err+0.0625f*neu->westn->northn->err+0.5625f*neu->err;
		neu->ch[2]->errpr=0.1875f*neu->westn->err+0.1875f*neu->southn->err+0.0625f*neu->westn->southn->err+0.5625f*neu->err;
		neu->ch[3]->errpr=0.1875f*neu->eastn->err+0.1875f*neu->southn->err+0.0625f*neu->eastn->southn->err+0.5625f*neu->err;
	}
//one edge has not got neighbor
	if(neu->eastn == 0 && neu->westn != 0 && neu->northn != 0 && neu->southn != 0)
	{	neu->ch[0]->errpr=0.1875f*0				 +0.1875f*neu->northn->err+0.0625f*0					  +0.5625f*neu->err;
		neu->ch[1]->errpr=0.1875f*neu->westn->err+0.1875f*neu->northn->err+0.0625f*neu->westn->northn->err+0.5625f*neu->err;
		neu->ch[2]->errpr=0.1875f*neu->westn->err+0.1875f*neu->southn->err+0.0625f*neu->westn->southn->err+0.5625f*neu->err;
		neu->ch[3]->errpr=0.1875f*0				 +0.1875f*neu->southn->err+0.0625f*0					  +0.5625f*neu->err;
	}
	if(neu->eastn != 0 && neu->westn == 0 && neu->northn != 0 && neu->southn != 0)
	{	neu->ch[0]->errpr=0.1875f*neu->eastn->err+0.1875f*neu->northn->err+0.0625f*neu->eastn->northn->err+0.5625f*neu->err;
		neu->ch[1]->errpr=0.1875f*0				 +0.1875f*neu->northn->err+0.0625f*0					  +0.5625f*neu->err;
		neu->ch[2]->errpr=0.1875f*0				 +0.1875f*neu->southn->err+0.0625f*0					  +0.5625f*neu->err;
		neu->ch[3]->errpr=0.1875f*neu->eastn->err+0.1875f*neu->southn->err+0.0625f*neu->eastn->southn->err+0.5625f*neu->err;
	}
	if(neu->eastn != 0 && neu->westn != 0 && neu->northn == 0 && neu->southn != 0)
	{	neu->ch[0]->errpr=0.1875f*neu->eastn->err+0.1875f*0				  +0.0625f*0					  +0.5625f*neu->err;
		neu->ch[1]->errpr=0.1875f*neu->westn->err+0.1875f*0				  +0.0625f*0					  +0.5625f*neu->err;
		neu->ch[2]->errpr=0.1875f*neu->westn->err+0.1875f*neu->southn->err+0.0625f*neu->westn->southn->err+0.5625f*neu->err;
		neu->ch[3]->errpr=0.1875f*neu->eastn->err+0.1875f*neu->southn->err+0.0625f*neu->eastn->southn->err+0.5625f*neu->err;
	}
	if(neu->eastn != 0 && neu->westn != 0 && neu->northn != 0 && neu->southn == 0)
	{	neu->ch[0]->errpr=0.1875f*neu->eastn->err+0.1875f*neu->northn->err+0.0625f*neu->eastn->northn->err+0.5625f*neu->err;
		neu->ch[1]->errpr=0.1875f*neu->westn->err+0.1875f*neu->northn->err+0.0625f*neu->westn->northn->err+0.5625f*neu->err;
		neu->ch[2]->errpr=0.1875f*neu->westn->err+0.1875f*0				  +0.0625f*0					  +0.5625f*neu->err;
		neu->ch[3]->errpr=0.1875f*neu->eastn->err+0.1875f*0				  +0.0625f*0					  +0.5625f*neu->err;
	}
//two edges have not got neighbor
	if(neu->eastn == 0 && neu->westn != 0 && neu->northn == 0 && neu->southn != 0)
	{	neu->ch[0]->errpr=0.1875f*0				 +0.1875f*0				  +0.0625f*0					  +0.5625f*neu->err;
		neu->ch[1]->errpr=0.1875f*neu->westn->err+0.1875f*0				  +0.0625f*0					  +0.5625f*neu->err;
		neu->ch[2]->errpr=0.1875f*neu->westn->err+0.1875f*neu->southn->err+0.0625f*neu->westn->southn->err+0.5625f*neu->err;
		neu->ch[3]->errpr=0.1875f*0				 +0.1875f*neu->southn->err+0.0625f*0					  +0.5625f*neu->err;
	}
	if(neu->eastn != 0 && neu->westn == 0 && neu->northn != 0 && neu->southn == 0)
	{	neu->ch[0]->errpr=0.1875f*neu->eastn->err+0.1875f*neu->northn->err+0.0625f*neu->eastn->northn->err+0.5625f*neu->err;
		neu->ch[1]->errpr=0.1875f*0				 +0.1875f*neu->northn->err+0.0625f*0					  +0.5625f*neu->err;
		neu->ch[2]->errpr=0.1875f*0				 +0.1875f*0				  +0.0625f*0					  +0.5625f*neu->err;
		neu->ch[3]->errpr=0.1875f*neu->eastn->err+0.1875f*0				  +0.0625f*0					  +0.5625f*neu->err;
	}
	if(neu->eastn != 0 && neu->westn == 0 && neu->northn == 0 && neu->southn != 0)
	{	neu->ch[0]->errpr=0.1875f*neu->eastn->err+0.1875f*0				  +0.0625f*0					  +0.5625f*neu->err;
		neu->ch[1]->errpr=0.1875f*0				 +0.1875f*0				  +0.0625f*0					  +0.5625f*neu->err;
		neu->ch[2]->errpr=0.1875f*0				 +0.1875f*neu->southn->err+0.0625f*0					  +0.5625f*neu->err;
		neu->ch[3]->errpr=0.1875f*neu->eastn->err+0.1875f*neu->southn->err+0.0625f*neu->eastn->southn->err+0.5625f*neu->err;
	}
	if(neu->eastn == 0 && neu->westn != 0 && neu->northn != 0 && neu->southn == 0)
	{	neu->ch[0]->errpr=0.1875f*0				 +0.1875f*neu->northn->err+0.0625f*0					  +0.5625f*neu->err;
		neu->ch[1]->errpr=0.1875f*neu->westn->err+0.1875f*neu->northn->err+0.0625f*neu->westn->northn->err+0.5625f*neu->err;
		neu->ch[2]->errpr=0.1875f*neu->westn->err+0.1875f*0				  +0.0625f*0					  +0.5625f*neu->err;
		neu->ch[3]->errpr=0.1875f*0				 +0.1875f*0				  +0.0625f*0					  +0.5625f*neu->err;
	}

}
}
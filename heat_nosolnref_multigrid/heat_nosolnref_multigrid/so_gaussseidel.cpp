#include "meshgen.h"

void meshgen::so_gaussseidel(cell *neu, int st)
{	
	if(neu->level != (unidiv-st))
	{
		for(int i=0;i<4;i++)
		so_gaussseidel(neu->ch[i],st);
	}

if(neu->level == unidiv-st)
{	float eeast=0.0f, ewest=0.0f, enorth=0.0f, esouth=0.0f;
// calculate eeast
		if(neu->eastn == 0)
			eeast=0.0f; 
		else
			eeast=*neu->ae*(neu->eastn->err); 
// calculate ewest
		if(neu->westn == 0)
			ewest=0.0f;
		else
			ewest=*neu->aw*(neu->westn->err); 
// calculate enorth
		if(neu->northn == 0)
			enorth=0.0f; 
		else
			enorth=*neu->an*(neu->northn->err); 
// calculate esouth
		if(neu->southn == 0)
			esouth=0.0f;
		else
			esouth=*neu->as*(neu->southn->err); 

	neu->err=(neu->resid+eeast+ewest+enorth+esouth)/(*neu->ap);

}
}
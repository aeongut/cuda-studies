#include "meshgen.h"

void meshgen::so_rk4(cell *nw)
{
float k1(0),k2(0),k3(0),k4(0),rs,CFL;
float temp;

temp=nw->temp;
CFL=(6.0f*nw->length)/20.0f; 

rs=so_Rcalc(nw);
k1= CFL *rs;
nw->temp=temp + (1.0f/2.0f) * k1;
	
rs=so_Rcalc(nw);
k2= CFL * rs;
nw->temp=temp + (1.0f/2.0f) * k2;
	
rs=so_Rcalc(nw);
k3= CFL *rs;	
nw->temp=temp + (1.0f) * k2;

rs=so_Rcalc(nw);
k4= CFL *rs;
nw->temp=temp + (k1 + 2.0f*k2 + 2.0f*k3 + k4) / 6.0f;

if(fabs(nw->temp - temp) > res)	
	res=fabs(nw->temp - temp);
}
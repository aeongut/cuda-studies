#include "meshgen.h"

float meshgen::so_Rcalc(cell *neu)
{ float Teast=0., Twest=0., Tnorth=0., Tsouth=0.; 

// calculate Teast
		if(neu->eastn == 0)
		{ Teast=2.0f*(bt[0]-neu->temp)/neu->length; 
		}
		else
		{		if(neu->level == (neu->eastn->level+1) )  
				Teast= (neu->eastn->temp-neu->temp)/(neu->eastn->xcent-neu->xcent);
				else if(neu->level == neu->eastn->level)
						{   if(neu->eastn->ch[0] == 0)
							Teast= (neu->eastn->temp-neu->temp)/(neu->eastn->xcent-neu->xcent); 
						    else if(neu->eastn->ch[0] != 0)
								{float t;
								t=(neu->eastn->ch[1]->temp + neu->eastn->ch[2]->temp)/2.0f;
								Teast= (t-neu->temp)/(3.0f*neu->length/4.0f);
								}
							else cout<<"wrong neighbor property was entered1\n";
						}
				else cout<<"wrong neighbor property was entered2\n";
		}

// calculate Twest
		if(neu->westn == 0)
		{ Twest=2.0f*(neu->temp-bt[1])/neu->length;
		}
		else
		{		if(neu->level == (neu->westn->level+1) )  
				Twest= (neu->temp-neu->westn->temp)/(neu->xcent-neu->westn->xcent);  
				else if(neu->level == neu->westn->level)
						{   if(neu->westn->ch[0] == 0)
							Twest= (neu->temp-neu->westn->temp)/(neu->xcent-neu->westn->xcent); 
						    else if(neu->westn->ch[0] != 0)
								{float t;
								t=(neu->westn->ch[0]->temp + neu->westn->ch[3]->temp)/2.0f;
								Twest= (neu->temp-t)/(3.0f*neu->length/4.0f);
								}
							else cout<<"wrong neighbor property was entered3\n";
						}
				else cout<<"wrong neighbor property was entered4\n";
		}

// calculate Tnorth
		if(neu->northn == 0)
		{ Tnorth=2.0f*(bt[2]-neu->temp)/neu->length;
		}
		else
		{		if(neu->level == (neu->northn->level+1) )  
				Tnorth= (neu->northn->temp-neu->temp)/(neu->northn->ycent-neu->ycent); 
				else if(neu->level == neu->northn->level)
						{   if(neu->northn->ch[0] == 0)
							Tnorth= (neu->northn->temp-neu->temp)/(neu->northn->ycent-neu->ycent); 
						    else if(neu->northn->ch[0] != 0)
								{float t;
								t=(neu->northn->ch[2]->temp + neu->northn->ch[3]->temp)/2.0f;
								Tnorth= (t-neu->temp)/(3.0f*neu->length/4.0f);
								}
							else cout<<"wrong neighbor property was entered5\n";
						}
				else cout<<"wrong neighbor property was entered6\n";
		}

// calculate Tsouth
		if(neu->southn == 0)
		{ Tsouth=2.0f*(neu->temp-bt[3])/neu->length;
		}
		else
		{		if(neu->level == (neu->southn->level+1) )  
				Tsouth= (neu->temp-neu->southn->temp)/(neu->ycent-neu->southn->ycent);
				else if(neu->level == neu->southn->level)
						{   if(neu->southn->ch[0] == 0)
							Tsouth= (neu->temp-neu->southn->temp)/(neu->ycent-neu->southn->ycent); 
						    else if(neu->southn->ch[0] != 0)
								{float t;
								t=(neu->southn->ch[0]->temp + neu->southn->ch[1]->temp)/2.0f;
								Tsouth= (neu->temp-t)/(3.0f*neu->length/4.0f);
								}
							else cout<<"wrong neighbor property was entered7\n";
						}
				else cout<<"wrong neighbor property was entered8\n";
		}

return (Teast-Twest+Tnorth-Tsouth);
}
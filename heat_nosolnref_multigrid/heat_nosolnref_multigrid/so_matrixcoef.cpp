#include "meshgen.h"

void meshgen::so_matrixcoef(cell *neu, int st)
{	
	if(neu->level != (unidiv-st))
	{
		for(int i=0;i<4;i++)
		so_matrixcoef(neu->ch[i],st);
	}

if(neu->level == unidiv-st)
{
		neu->ap=new float(0);
		neu->ae=new float(0);
		neu->aw=new float(0);
		neu->an=new float(0);
		neu->as=new float(0);

// calculate e_er
		if(neu->eastn == 0)
			*neu->ap=*neu->ap+2.0f/neu->length; 
		else
		{	*neu->ae=1.0f/(neu->eastn->xcent-neu->xcent); 
			*neu->ap=*neu->ap+1.0f/(neu->eastn->xcent-neu->xcent);
		}
// calculate w_er
		if(neu->westn == 0)
			*neu->ap=*neu->ap+2.0f/neu->length; 
		else
		{	*neu->aw=1.0f/(neu->xcent-neu->westn->xcent); 
			*neu->ap=*neu->ap+1.0f/(neu->xcent-neu->westn->xcent); 
		}
// calculate n_er
		if(neu->northn == 0)
			*neu->ap=*neu->ap+2.0f/neu->length;
		else
		{	*neu->an=1.0f/(neu->northn->ycent-neu->ycent); 
			*neu->ap=*neu->ap+1.0f/(neu->northn->ycent-neu->ycent); 
		}
// calculate s_er
		if(neu->southn == 0)
			*neu->ap=*neu->ap+2.0f/neu->length;
		else
		{	*neu->as=1.0f/(neu->ycent-neu->southn->ycent); 
			*neu->ap=*neu->ap+1.0f/(neu->ycent-neu->southn->ycent); 
		}
}
}

#include "meshgen.h"

void meshgen::so_calcresid(cell *neu)
{	float Teast=0.0f, Twest=0.0f, Tnorth=0.0f, Tsouth=0.0f; 

	if(neu->ch[0] != 0)
	{
		for(int i=0;i<4;i++)
		so_calcresid(neu->ch[i]);
	}

if(neu->ch[0] == 0)
{
// calculate Teast
		if(neu->eastn == 0)
			Teast=2.0f*(bt[0]-neu->temp)/neu->length; 
		else
			Teast=(neu->eastn->temp-neu->temp)/(neu->eastn->xcent-neu->xcent); 
// calculate Twest
		if(neu->westn == 0)
			Twest=2.0f*(neu->temp-bt[1])/neu->length;
		else
			Twest=(neu->temp-neu->westn->temp)/(neu->xcent-neu->westn->xcent); 
// calculate Tnorth
		if(neu->northn == 0)
			Tnorth=2.0f*(bt[2]-neu->temp)/neu->length;
		else
			Tnorth=(neu->northn->temp-neu->temp)/(neu->northn->ycent-neu->ycent); 
// calculate Tsouth
		if(neu->southn == 0)
			Tsouth=2.0f*(neu->temp-bt[3])/neu->length;
		else
			Tsouth=(neu->temp-neu->southn->temp)/(neu->ycent-neu->southn->ycent); 

neu->resid=0.0f-(-Teast+Twest-Tnorth+Tsouth);
}
}
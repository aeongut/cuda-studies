#include	"cu_def.h"
#include	"cu_cell.h"

extern d_cell*	d_list;
extern size_t	pitch;

extern "C" void cu_fetch(cell	**liste,		// SFClist on host
						 int	numleaf)
{

	d_cell *h_list = (d_cell*) malloc (numleaf * sizeof(cell));
	cudaMemcpy2D (h_list, sizeof(d_cell), d_list, pitch, sizeof(d_cell), numleaf, cudaMemcpyDeviceToHost);

	for (int i=0; i<numleaf; i++)
	{
		liste[i]->temp	= h_list[i].temp;
		liste[i]->grad	= h_list[i].grad;
	}
}

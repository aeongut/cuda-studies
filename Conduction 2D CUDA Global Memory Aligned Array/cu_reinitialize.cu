#include	"cu_def.h"
#include	"cu_cell.h"

extern d_cell*	d_list;
extern size_t	pitch;

extern "C" void cu_reinitialize(cell	**liste,	// SFClist on host
								int		numleaf)	// Number of cells on host
{

/// cell pointer list SFClist is converted to a d_cell list
	d_cell *h_list = (d_cell*) malloc(numleaf * sizeof(cell));
	for (int i=0; i<numleaf; i++)
	{
		h_list[i].xy[0]		= liste[i]->xcent;
		h_list[i].xy[1]		= liste[i]->ycent;
		h_list[i].length	= liste[i]->length;
		h_list[i].temp		= liste[i]->temp;
		h_list[i].grad		= liste[i]->grad;
		for (int j=0; j<8; j++)
			h_list[i].SFCng[j]	= liste[i]->SFCng[j];
	}

/// h_list is copied to device
	cudaMallocPitch ((void**)&d_list, &pitch, sizeof(d_cell), numleaf);
	cudaMemcpy2D (d_list, pitch, h_list, sizeof(d_cell), sizeof(d_cell), numleaf, cudaMemcpyHostToDevice);
}


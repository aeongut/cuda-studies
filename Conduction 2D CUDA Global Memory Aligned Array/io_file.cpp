#include "meshgen.h"

void meshgen::io_file()
{
out.open("tec.dat",ios::out);
out<<"Title: \"2-D Heat Conduction Problem Solver with using Cartesian Grid\" \n";
out<<"variables=x,y,T \n";
out<<"zone t=\"level 0\" i="<<numleaf*4<<", j="<<numleaf<<", f=fepoint et=quadrilateral \n";

}

#include	"cu_def.h"
#include	"cu_cell.h"

extern d_cell*	d_list;
extern float*	d_bt;
extern float*	d_res;
extern float*	h_res;
extern float*	d_maxgrad;
extern float*	h_maxgrad;

extern "C" void cu_finalize()
{

	cudaFree(d_list);
	cudaFree(d_bt);
	cudaFree(d_res);
	cudaFreeHost(h_res);
	cudaFree(d_maxgrad);
	cudaFreeHost(h_maxgrad);
}


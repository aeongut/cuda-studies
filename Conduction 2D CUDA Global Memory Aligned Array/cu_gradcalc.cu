#include	"cu_def.h"
#include	"cu_cell.h"
#include	"gradcalc.kernel"

extern d_cell*	d_list;
extern float*	d_maxgrad;
extern float*	d_bt;
extern size_t	pitch;

extern "C" void cu_gradcalc(int numleaf)
{
	
	cudaMemset(d_maxgrad, 0, sizeof(float));

	dim3 dimBlock(threadsPerBlock,1,1);
	dim3 dimGrid((numleaf+threadsPerBlock-1)/threadsPerBlock,1);
	gradcalc_kernel<<<dimGrid, dimBlock>>>(numleaf, d_list, pitch, d_bt, d_maxgrad);

}
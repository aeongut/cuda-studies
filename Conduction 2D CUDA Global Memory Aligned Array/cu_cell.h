struct __align__(16) d_cell
{
	int			SFCng[8];
	float		xy[2];
	float		length;
	float		temp, grad;
};
typedef struct d_cell d_cell;
#include	"cu_def.h"

extern float*		h_res;
extern float*		d_res;
extern cudaStream_t stream2;

extern "C" float* cu_getres()
{	
	cudaMemcpyAsync(h_res, d_res, sizeof(float), cudaMemcpyDeviceToHost, stream2);
	//cudaMemcpy(h_res, d_res, sizeof(float), cudaMemcpyDeviceToHost);

	return h_res;
}

#include "meshgen.h"

 // firstly root is sent to this subroutine
 // and this subroutine creates child of the child of the sended cell
 // if you send root, by this subroutine root.ch.ch is produced.

void meshgen::mg_uniformmesh(cell *neu) 
{
	for(int i=0;i<4;i++)
		 mg_createchild(neu->ch[i]);    //level 2  

	if(neu->level<(unidiv-2))
			for(int i=0;i<4;i++)
				mg_uniformmesh(neu->ch[i]);  //level 3 and so on...

}

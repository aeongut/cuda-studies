#include "meshgen.h"

void meshgen::so_solve()
{ 
resid.open("res.dat",ios::out);
int i=0;
float inres=0.;
do
  {   res=0.;
      i=i+1;
	  so_unisolve(root);
	  if(i==1) inres=res;

	  if(res <= 0.1*inres)
		  {	  inres=res;
			  maxgrad=0.;
			  so_gradcalc(root);
			  so_findrefcell(root);
			  so_refine(root);
			  cout<<"refinement was performed\n";
		  }

	  if(i%50==0)
      cout<<i<<'\t'<<res<<endl;
      resid<<i<<'\t'<<log10(res)<<endl;

  } while(res>=1E-6);
  cout<<i<<'\t'<<res<<endl;
}
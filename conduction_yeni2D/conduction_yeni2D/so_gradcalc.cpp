#include "meshgen.h"

void meshgen::so_gradcalc(cell *neu)
{
float egrad=0., wgrad=0., ngrad=0., sgrad=0.; 
	if(neu->ch[0] != 0)
	{
		for(int i=0;i<4;i++)
		so_gradcalc(neu->ch[i]);
	}

if(neu->ch[0] == 0)
{
// East Gradient Calculation
		if(neu->ng[0] == 0)
		{ egrad=2*(bt[0]-*neu->temp)/neu->length;
		}
		else
		{		if(neu->level == (neu->ng[0]->level+1) )  
				egrad= (*neu->ng[0]->temp-*neu->temp)/(neu->ng[0]->xcent-neu->xcent);  
				else if(neu->level == neu->ng[0]->level)
						{   if(neu->ng[0]->ch[0] == 0)
							egrad= (*neu->ng[0]->temp-*neu->temp)/(neu->ng[0]->xcent-neu->xcent); 
						    else if(neu->ng[0]->ch[0] != 0)
								{float t;
								t=(*neu->ng[0]->ch[1]->temp + *neu->ng[0]->ch[2]->temp)/2;
								egrad= (t-*neu->temp)/(3*neu->length/4);
								}
							else cout<<"wrong neighbor property was entered/n";
						}
				else cout<<"wrong neighbor property was entered/n";
		}

// West Gradient Calculation
		if(neu->ng[2] == 0)
		{ wgrad=2*(*neu->temp-bt[2])/neu->length;
		}
		else
		{		if(neu->level == (neu->ng[2]->level+1) )  
				wgrad= (*neu->temp-*neu->ng[2]->temp)/(neu->xcent-neu->ng[2]->xcent);   
				else if(neu->level == neu->ng[2]->level)
						{   if(neu->ng[2]->ch[0] == 0)
							wgrad= (*neu->temp-*neu->ng[2]->temp)/(neu->xcent-neu->ng[2]->xcent); 
						    else if(neu->ng[2]->ch[0] != 0)
								{float t;
								t=(*neu->ng[2]->ch[0]->temp + *neu->ng[2]->ch[3]->temp)/2;
								wgrad= (*neu->temp-t)/(3*neu->length/4);
								}
							else cout<<"wrong neighbor property was entered/n";
						}
				else cout<<"wrong neighbor property was entered/n";
		}

// North Gradient Calculation
		if(neu->ng[1] == 0)
		{ ngrad=2*(bt[1]-*neu->temp)/neu->length;
		}
		else
		{		if(neu->level == (neu->ng[1]->level+1) )  
				ngrad= (*neu->ng[1]->temp-*neu->temp)/(neu->ng[1]->ycent-neu->ycent);   
				else if(neu->level == neu->ng[1]->level)
						{   if(neu->ng[1]->ch[0] == 0)
							ngrad= (*neu->ng[1]->temp-*neu->temp)/(neu->ng[1]->ycent-neu->ycent); 
						    else if(neu->ng[1]->ch[0] != 0)
								{float t;
								t=(*neu->ng[1]->ch[2]->temp + *neu->ng[1]->ch[3]->temp)/2;
								ngrad= (t-*neu->temp)/(3*neu->length/4);
								}
							else cout<<"wrong neighbor property was entered/n";
						}
				else cout<<"wrong neighbor property was entered/n";
		}

// South Gradient Calculation
		if(neu->ng[3] == 0)
		{ sgrad=2*(*neu->temp-bt[3])/neu->length;
		}
		else
		{		if(neu->level == (neu->ng[3]->level+1) )  
				sgrad= (*neu->temp-*neu->ng[3]->temp)/(neu->ycent-neu->ng[3]->ycent);   
				else if(neu->level == neu->ng[3]->level)
						{   if(neu->ng[3]->ch[0] == 0)
							sgrad= (*neu->temp-*neu->ng[3]->temp)/(neu->ycent-neu->ng[3]->ycent); 
						    else if(neu->ng[3]->ch[0] != 0)
								{float t;
								t=(*neu->ng[3]->ch[0]->temp + *neu->ng[3]->ch[1]->temp)/2;
								sgrad= (*neu->temp-t)/(3*neu->length/4);
								}
							else cout<<"wrong neighbor property was entered/n";
						}
				else cout<<"wrong neighbor property was entered/n";
		}

*neu->grad=sqrt(pow(egrad,2)+pow(wgrad,2)+pow(ngrad,2)+pow(sgrad,2));
// finding maximum gradient amongst leafcells
if(*neu->grad > maxgrad)
maxgrad = *neu->grad;
}
}

#include "meshgen.h"

void meshgen::mg_centercoord(int cellnum, cell *neu)
{
// bne
if(cellnum==0)
   { neu->xcent=neu->parent->xcent+domsize/pow(2.0,neu->level+1);
     neu->ycent=neu->parent->ycent+domsize/pow(2.0,neu->level+1);

   }
// bnw
else if(cellnum==1)
   { neu->xcent=neu->parent->xcent-domsize/pow(2.0,neu->level+1);
     neu->ycent=neu->parent->ycent+domsize/pow(2.0,neu->level+1);

   }
// bsw
 else if(cellnum==2)
   { neu->xcent=neu->parent->xcent-domsize/pow(2.0,neu->level+1);
     neu->ycent=neu->parent->ycent-domsize/pow(2.0,neu->level+1);

   }
// bse
else if(cellnum==3)
   { neu->xcent=neu->parent->xcent+domsize/pow(2.0,neu->level+1);
     neu->ycent=neu->parent->ycent-domsize/pow(2.0,neu->level+1);

   }

else
   { cout<<"\n wrong cell number\n\n";
	 exit(EXIT_FAILURE);
   }
}

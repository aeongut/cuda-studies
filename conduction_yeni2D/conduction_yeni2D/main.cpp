#include "meshgen.h"

int main()
{
	float minibt;
	meshgen gr;

	minibt=gr.io_input();

	gr.mg_createroot();

	gr.so_initcells(minibt,gr.root);
	gr.so_solve();

	gr.io_countcells(gr.root);
	gr.io_file();
	gr.io_tecplot(gr.root);
	gr.io_tecplot2();

	getchar();
	return 0;
}
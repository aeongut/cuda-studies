#include "meshgen.h"

void meshgen::mg_createchild(cell *nw)
{
cell *child[4];

for(int i=0;i<4;i++)
  {
    child[i]=new cell;
	nw->ch[i]=child[i];
	mg_forminit(nw->ch[i]);
	nw->ch[i]->parent=nw;
	nw->ch[i]->level=nw->level+1;
	nw->ch[i]->length=nw->length/2;
	mg_centercoord(i,nw->ch[i]);
  }

}
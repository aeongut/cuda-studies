#ifndef DATASTRUCT
#define DATASTRUCT

class cell 
{	public:

float xcent, ycent;
int level;
float length;
float *temp, *grad;
int *ref;
cell	*ng[4];	// ng[0]=east neigh, ng[1]=north neigh, ng[2]=west neigh, ng[3]=south neigh
cell *ch[4];
cell *parent;
};

#endif
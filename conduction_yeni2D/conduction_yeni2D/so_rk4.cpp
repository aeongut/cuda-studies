#include "meshgen.h"

void meshgen::so_rk4(cell *nw)
{
float k1(0),k2(0),k3(0),k4(0),rs,CFL;
float temp;

temp=*nw->temp;
CFL=(3.*nw->length)/40; 

rs=so_Rcalc(nw);
k1= CFL *rs;
*nw->temp=temp + (1.0/2.0) * k1;
	
rs=so_Rcalc(nw);
k2= CFL * rs;
*nw->temp=temp + (1.0/2.0) * k2;
	
rs=so_Rcalc(nw);
k3= CFL *rs;	
*nw->temp=temp + (1.0) * k3;

rs=so_Rcalc(nw);
k4= CFL *rs;
*nw->temp=temp + (k1 + 2*k2 + 2*k3 + k4) / 6;

if(fabs(*nw->temp - temp) > res)	
	res=fabs(*nw->temp - temp);
}
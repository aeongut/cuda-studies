#include "meshgen.h"

void meshgen::mg_createroot()
{
int j=0;
root=new cell;    // level 0
mg_forminit(root);
root->xcent= 0.;
root->ycent= 0.;
root->level=0;
root->length=domsize;

mg_createchild(root);  // level 1
mg_uniformmesh(root);

for(int i=0;i<4;i++)
 		mg_setneighbors(i,root->ch[i]); 


}
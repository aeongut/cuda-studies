# README #

This repository contains my experiments on GPU using CUDA. The original code for cpu is written by one of my former colleagues in METU so design don't belong to me :)) Don't blame it to me if you accidentally get lost in the files ;)

### What do we have here?
We have multiple folders containing different memory management strategies to get maximum performance from CUDA. To be honest I don't even remember what was what but still these can be useful for someone who is interested in CUDA. Or to me if I ever get back to GPGPU programming.
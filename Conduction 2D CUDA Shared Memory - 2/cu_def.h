#ifndef _CU_DEF_H_
#define _CU_DEF_H_

#include	<stdio.h>
#include	<stdlib.h>
#include	<math.h>
#include	"cuda.h"
#include	"datastruct.h"


#define threadsPerBlock 16

typedef struct cell cell;

extern "C" void cu_initialize(cell**, int, float*, cell**, size_t&, float**, float**, float**, float**, float**);
extern "C" void cu_reinitialize(cell**, int, cell**, size_t&);
extern "C" void cu_RKsolver(int, cell*, size_t,  float*, float*);
extern "C" void cu_gradcalc(int, cell*, size_t,  float*, float*);
extern "C" void cu_fetch(cell**, int, cell*, size_t);
extern "C" void cu_getres(float*, float*);
extern "C" void cu_getmaxgrad(float*, float*);

#endif

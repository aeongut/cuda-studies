#include "meshgen.h"

int main()
{

	float minibt;
	meshgen gr;
	gr.io_timer(0);
	minibt=gr.io_input();
	gr.mg_createroot();
	gr.so_initcells(minibt,gr.root);
	gr.mg_SFCcontrol(gr.root);

	gr.so_cudacontrol();

	gr.numleaf = 0;
	gr.io_countcells(gr.root);
	gr.io_file();
	gr.io_tecplot(gr.root);
	gr.io_tecplot2();
	
	cout << "Number of cells : " << gr.numleaf << endl;
	gr.io_timer(1);

	getchar();
	return 0;
}	

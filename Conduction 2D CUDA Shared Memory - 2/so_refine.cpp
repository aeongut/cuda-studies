#include "meshgen.h"

void meshgen::so_refine(cell *neu)
{
	if(neu->ch[0] != 0)
	{
		for(int i=0;i<4;i++)
		so_refine(neu->ch[i]);
	}
	else
	{
		if(*neu->ref==1)
		{
			for(int i=0; i<4; i++)
			{	if(neu->ng[i]!=0)
				{	if(neu->ng[i]->level < neu->level)
					{	
						*neu->ng[i]->ref = 1;
						so_refine(neu->ng[i]);
					}
				}
			}
			mg_createchild(neu);
			for(int i=0;i<4;i++)
			{	
				neu->ch[i]->temp=neu->temp;
				neu->ch[i]->grad=0;
				neu->ch[i]->ref=new int(0);
				mg_setneighbors(i,neu->ch[i]);
			}
		neu->temp = 0;
		neu->grad = 0;
		neu->ref = NULL;
		}
	}
}


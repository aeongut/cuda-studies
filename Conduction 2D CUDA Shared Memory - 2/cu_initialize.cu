#include	"cu_def.h"

extern "C" void cu_initialize(cell		**liste,		// SFClist on host
							  int		numleaf,		// Number of cells on host
							  float		*h_bt,			// Boundary values from host
							  cell		**d_list,		// *d_list is the address of the device data structure
							  size_t	&pitch,			// pitch is the pitch of the device data structure
							  float		**d_bt,			// *d_bt is the address of the device boundary values
							  float		**d_res,		// *d_rs is the address of the device residual
							  float		**h_res,		// *d_inres is the address of the kept device residual
							  float		**d_maxgrad,	// *d_maxgrad is the address of the device maximum grad
							  float		**h_maxgrad)	// *h_maxgrad is the address of the host maximum grad
{
/// cell pointer list SFClist is converted to a cell list
	cell *h_list = (cell*) malloc(numleaf * sizeof(cell));
	for (int i=0; i<numleaf; i++)
		h_list[i] = *liste[i];

/// alist is copied to device

	cudaMallocPitch ((void**)d_list, &pitch, sizeof(cell), numleaf);
	cudaMemcpy2D (*d_list, pitch, h_list, sizeof(cell), sizeof(cell), numleaf, cudaMemcpyHostToDevice);

//	Eri�im �ekli:
//	cell *neu = (cell*)((char*)*d_list+0*pitch);
//	printf("%d\n",neu->level);

/// boundary temperature is sent to device
	cudaMalloc ((void**)d_bt, 4*sizeof(float));
	cudaMemcpy (*d_bt, h_bt, 4*sizeof(float), cudaMemcpyHostToDevice);

/// memory allocated on device for residual, maximum gradient, host residual 
	cudaMalloc ((void**)d_res, sizeof(float));
	cudaMalloc ((void**)d_maxgrad, sizeof(float));
	cudaMallocHost ((void**)h_res, sizeof(int));
	cudaMallocHost ((void**)h_maxgrad, sizeof(int));
}


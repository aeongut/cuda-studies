#include	"cu_def.h"
#include	"gradcalc.kernel"

extern "C" void cu_gradcalc(int numleaf, cell *d_list, size_t pitch, float *d_bt, float *d_maxgrad)
{
	cudaMemset(d_maxgrad, 0, sizeof(float));

	dim3 dimBlock(threadsPerBlock,1,1);
	dim3 dimGrid((numleaf+threadsPerBlock-1)/threadsPerBlock,1);
	gradcalc_kernel<<<dimGrid, dimBlock>>>(numleaf, d_list, pitch, d_bt, d_maxgrad);

}
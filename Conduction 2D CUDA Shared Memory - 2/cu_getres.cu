#include	"cu_def.h"

extern "C" void cu_getres(float* d_res, float* h_res)
{	
	cudaMemcpy(h_res, d_res, sizeof(float), cudaMemcpyDeviceToHost);
}

#include "meshgen.h"

void meshgen::mg_setneighbors(int cellnum, cell *neu)
{
// ne
if(cellnum==0)
{ 
neu->ng[3]=neu->parent->ch[3];
neu->ng[2]=neu->parent->ch[1];

      if(neu->parent->ng[1]!= 0 && neu->parent->ng[1]->ch[0]!= 0)
	  {  neu->ng[1]=neu->parent->ng[1]->ch[3];
	     neu->parent->ng[1]->ch[3]->ng[3]=neu;
	  }
	  if(neu->parent->ng[1]!= 0 && neu->parent->ng[1]->ch[0]== 0)
	  neu->ng[1]=neu->parent->ng[1];

			if(neu->parent->ng[0]!= 0 && neu->parent->ng[0]->ch[0]!= 0)
			{	neu->ng[0]=neu->parent->ng[0]->ch[1];
			    neu->parent->ng[0]->ch[1]->ng[2]=neu;
			}
	        if(neu->parent->ng[0]!= 0 && neu->parent->ng[0]->ch[0]== 0)
		    neu->ng[0]=neu->parent->ng[0];
}
// nw
else if(cellnum==1)
{ 
neu->ng[3]=neu->parent->ch[2];
neu->ng[0]=neu->parent->ch[0];

      if(neu->parent->ng[1]!= 0 && neu->parent->ng[1]->ch[0]!= 0)
	  {  neu->ng[1]=neu->parent->ng[1]->ch[2];
	     neu->parent->ng[1]->ch[2]->ng[3]=neu;
	  }
	  if(neu->parent->ng[1]!= 0 && neu->parent->ng[1]->ch[0]== 0)
	  neu->ng[1]=neu->parent->ng[1];

			if(neu->parent->ng[2]!= 0 && neu->parent->ng[2]->ch[0]!= 0)
			{	neu->ng[2]=neu->parent->ng[2]->ch[0];
			    neu->parent->ng[2]->ch[0]->ng[0]=neu;
			}
	        if(neu->parent->ng[2]!= 0 && neu->parent->ng[2]->ch[0]== 0)
		    neu->ng[2]=neu->parent->ng[2];
}

// sw
 else if(cellnum==2)
{ 
neu->ng[1]=neu->parent->ch[1];
neu->ng[0]=neu->parent->ch[3];

      if(neu->parent->ng[3]!= 0 && neu->parent->ng[3]->ch[0]!= 0)
	  {  neu->ng[3]=neu->parent->ng[3]->ch[1];
	     neu->parent->ng[3]->ch[1]->ng[1]=neu;
	  }
	  if(neu->parent->ng[3]!= 0 && neu->parent->ng[3]->ch[0]== 0)
	  neu->ng[3]=neu->parent->ng[3];

			if(neu->parent->ng[2]!= 0 && neu->parent->ng[2]->ch[0]!= 0)
			{	neu->ng[2]=neu->parent->ng[2]->ch[3];
			    neu->parent->ng[2]->ch[3]->ng[0]=neu;
			}
	        if(neu->parent->ng[2]!= 0 && neu->parent->ng[2]->ch[0]== 0)
		    neu->ng[2]=neu->parent->ng[2];
}

// se
else if(cellnum==3)
{
neu->ng[1]=neu->parent->ch[0];
neu->ng[2]=neu->parent->ch[2];

      if(neu->parent->ng[3]!= 0 && neu->parent->ng[3]->ch[0]!= 0)
	  {  neu->ng[3]=neu->parent->ng[3]->ch[0];
	     neu->parent->ng[3]->ch[0]->ng[1]=neu;
	  }
	  if(neu->parent->ng[3]!= 0 && neu->parent->ng[3]->ch[0]== 0)
	  neu->ng[3]=neu->parent->ng[3];

			if(neu->parent->ng[0]!= 0 && neu->parent->ng[0]->ch[0]!= 0)
			{	neu->ng[0]=neu->parent->ng[0]->ch[2];
			    neu->parent->ng[0]->ch[2]->ng[2]=neu;
			}
	        if(neu->parent->ng[0]!= 0 && neu->parent->ng[0]->ch[0]== 0)
		    neu->ng[0]=neu->parent->ng[0];
}

else
   {
	cout<<"\n wrong cell number\n\n";
	getchar();
	exit(EXIT_FAILURE);
   }

if(neu->level>0 && neu->level<unidiv)
for(int i=0;i<4;i++)
mg_setneighbors(i,neu->ch[i]);
}

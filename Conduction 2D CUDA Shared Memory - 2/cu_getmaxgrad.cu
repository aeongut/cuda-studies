#include	"cu_def.h"

extern "C" void cu_getmaxgrad(float* d_maxgrad, float* h_maxgrad)
{	
	cudaMemcpy(h_maxgrad, d_maxgrad, sizeof(float), cudaMemcpyDeviceToHost);
}

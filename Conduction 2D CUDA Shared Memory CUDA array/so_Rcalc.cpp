#include "meshgen.h"

float meshgen::so_Rcalc(cell *neu)
{ 
	float Teast=0.0, Twest=0.0, Tnorth=0.0, Tsouth=0.0; 

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// calculate Teast 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	if(neu->ng[0] == 0)
		Teast=2*(bt[0]-*neu->temp)/neu->length; 
	else
	{
		if(neu->level == (neu->ng[0]->level+1) )
			Teast= (*neu->ng[0]->temp-*neu->temp)/(neu->ng[0]->xcent-neu->xcent);
		else if(neu->level == neu->ng[0]->level)
		{
			if(neu->ng[0]->ch[0] == 0)
				Teast= (*neu->ng[0]->temp-*neu->temp)/(neu->ng[0]->xcent-neu->xcent); 
		    else if(neu->ng[0]->ch[0] != 0)
				Teast= (((*neu->ng[0]->ch[1]->temp + *neu->ng[0]->ch[2]->temp)/2)-*neu->temp)/(3*neu->length/4);
			else cout<<"wrong neighbor property was entered1\n";
		}
		else cout<<"wrong neighbor property was entered2\n";
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// calculate Twest
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	if(neu->ng[2] == 0)
		Twest=2*(*neu->temp-bt[2])/neu->length;
	else
	{
		if(neu->level == (neu->ng[2]->level+1) )  
			Twest= (*neu->temp-*neu->ng[2]->temp)/(neu->xcent-neu->ng[2]->xcent);  
		else if(neu->level == neu->ng[2]->level)
		{
			if(neu->ng[2]->ch[0] == 0)
				Twest= (*neu->temp-*neu->ng[2]->temp)/(neu->xcent-neu->ng[2]->xcent); 
			 else if(neu->ng[2]->ch[0] != 0)
				Twest= (*neu->temp-((*neu->ng[2]->ch[0]->temp + *neu->ng[2]->ch[3]->temp)/2))/(3*neu->length/4);
			else cout<<"wrong neighbor property was entered3\n";
		}
		else cout<<"wrong neighbor property was entered4\n";
	}
//	if (neu->SFCorder == 0) cout << endl <<"CPU RK 1st step 1st cell Twest : "<< Twest << endl;	
//	if (neu->SFCorder == 0) cout << endl <<"CPU RK 1st step 1st cell T     : "<< *neu->temp << endl;	

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// calculate Tnorth
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////		if(neu->ng[1] == 0)
	if(neu->ng[1] == 0)
		Tnorth=2*(bt[1]-*neu->temp)/neu->length;
	else
	{
		if(neu->level == (neu->ng[1]->level+1) )  
			Tnorth= (*neu->ng[1]->temp-*neu->temp)/(neu->ng[1]->ycent-neu->ycent); 
		else if(neu->level == neu->ng[1]->level)
		{
			if(neu->ng[1]->ch[0] == 0)
				Tnorth= (*neu->ng[1]->temp-*neu->temp)/(neu->ng[1]->ycent-neu->ycent); 
		    else if(neu->ng[1]->ch[0] != 0)
				Tnorth= (((*neu->ng[1]->ch[2]->temp + *neu->ng[1]->ch[3]->temp)/2)-*neu->temp)/(3*neu->length/4);
			else cout<<"wrong neighbor property was entered5\n";
		}
		else cout<<"wrong neighbor property was entered6\n";
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// calculate Tsouth
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////		if(neu->ng[3] == 0)
	if(neu->ng[3] == 0)
		Tsouth=2*(*neu->temp-bt[3])/neu->length;
	else
	{
		if(neu->level == (neu->ng[3]->level+1) )  
			Tsouth= (*neu->temp-*neu->ng[3]->temp)/(neu->ycent-neu->ng[3]->ycent);
		else if(neu->level == neu->ng[3]->level)
		{
			if(neu->ng[3]->ch[0] == 0)
				Tsouth= (*neu->temp-*neu->ng[3]->temp)/(neu->ycent-neu->ng[3]->ycent); 
			else if(neu->ng[3]->ch[0] != 0)
				Tsouth= (*neu->temp-((*neu->ng[3]->ch[0]->temp + *neu->ng[3]->ch[1]->temp)/2))/(3*neu->length/4);
			else cout<<"wrong neighbor property was entered7\n";
		}
		else cout<<"wrong neighbor property was entered8\n";
	}

return (Teast-Twest+Tnorth-Tsouth);

}


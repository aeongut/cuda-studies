#include "meshgen.h"

void meshgen::mg_SFCinitialize(cell *neu)
{
	if(neu->ch[0] != 0)
	{
		neu->SFCorder	= 0;
		neu->SFCtype	= new int(0);
		for(int i=0;i<4;i++)
			mg_SFCinitialize(neu->ch[i]);
	}
	else
	{
		neu->SFCorder	= 0;
		neu->SFCtype	= new int(0);
	}
}

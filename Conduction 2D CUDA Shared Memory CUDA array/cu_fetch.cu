#include	"cu_def.h"

extern float* h_temp;
extern float* h_grad;
extern float* d_temp;
extern float* d_grad;

extern "C" void cu_fetch(cell	**liste,		// SFClist on host
						 int	numleaf)
{
	cudaMemcpy (h_temp, d_temp, numleaf*sizeof(float), cudaMemcpyDeviceToHost);
	cudaMemcpy (h_grad, d_grad, numleaf*sizeof(float), cudaMemcpyDeviceToHost);

	for (int i=0; i<numleaf; i++)
	{
		liste[i]->temp	= h_temp[i];
		liste[i]->grad	= h_grad[i];
	}
}

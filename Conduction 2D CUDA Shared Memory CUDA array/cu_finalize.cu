#include	"cu_def.h"

extern float*	d_bt;
extern float*	d_maxgrad;
extern float*	d_res;
extern float*	d_center;
extern float*	d_length;
extern float*	d_temp;
extern float*	d_grad;
extern int*		d_SFCng;

extern float*	h_maxgrad;
extern float*	h_res;
extern float*	h_center;
extern float*	h_length;
extern float*	h_temp;
extern float*	h_grad;
extern int*		h_SFCng;

extern "C" void cu_finalize()
{
	cudaFree(d_bt);
	cudaFree(d_res);
	cudaFree(d_maxgrad);
	cudaFree(d_center);
	cudaFree(d_length);
	cudaFree(d_temp);
	cudaFree(d_grad);
	cudaFree(d_SFCng);

	cudaFreeHost(h_res);
	cudaFreeHost(h_maxgrad);

	free(h_center);
	free(h_length);
	free(h_temp);
	free(h_grad);
	free(h_SFCng);
}


#include	"cu_def.h"
#include	"gradcalc.kernel"

extern float*	d_maxgrad;
extern float*	d_bt;

extern float*	d_center;
extern float*	d_length;
extern float*	d_temp;
extern float*	d_grad;
extern int*		d_SFCng;

extern "C" void cu_gradcalc(int numleaf)
{
	cudaMemset(d_maxgrad, 0, sizeof(float));

	dim3 dimBlock(threadsPerBlock,1,1);
	dim3 dimGrid((numleaf+threadsPerBlock-1)/threadsPerBlock,1);
	gradcalc_kernel<<<dimGrid, dimBlock>>>(numleaf, d_bt, d_maxgrad, d_center, d_length, d_temp, d_SFCng, d_grad);
}


#include	"cu_def.h"
#include	"RKsolver.kernel"

extern float*		d_bt;
extern float*		d_res;
extern cudaStream_t stream1;

	extern float*	d_center;
	extern float*	d_length;
	extern float*	d_temp;
	extern float*	d_grad;
	extern int*		d_SFCng;

extern "C" void cu_RKsolver(int numleaf)
{	
	cudaMemset(d_res, 0, sizeof(float));

	dim3 dimBlock(threadsPerBlock,1,1);
	dim3 dimGrid((numleaf+threadsPerBlock-1)/threadsPerBlock,1);

	RKsolver_kernel	<<<dimGrid, dimBlock, 0, stream1>>> (numleaf, d_bt, d_res, d_center, d_length, d_temp, d_SFCng);
//	fprintf(stderr, "%s", cudaGetErrorString (cudaGetLastError ()));
}


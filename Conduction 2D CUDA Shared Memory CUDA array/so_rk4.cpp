#include "meshgen.h"

void meshgen::so_rk4(cell *nw)
{
	float k[4], rs, CFL, temp;

	temp=*nw->temp;
	CFL=(3.0f*nw->length)/40.0f; 

	rs=so_Rcalc(nw);
	k[0]= CFL *rs;
	*nw->temp=temp + (1.0f/2.0f) * k[0];

	rs=so_Rcalc(nw);
	k[1]= CFL * rs;
	*nw->temp=temp + (1.0f/2.0f) * k[1];
		
	rs=so_Rcalc(nw);
	k[2]= CFL *rs;	
	*nw->temp=temp + (1.0f) * k[2];

	rs=so_Rcalc(nw);
	k[3]= CFL *rs;
	*nw->temp=temp + (k[0] + 2*k[1] + 2*k[2] + k[3]) / 6.0f;

	if(fabs(*nw->temp - temp) > res)	
		res=fabs(*nw->temp - temp);



}


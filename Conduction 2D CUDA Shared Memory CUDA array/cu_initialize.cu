#include	"cu_def.h"
#include	"cu_global.h"

extern "C" void cu_initialize(cell		**liste,		// SFClist on host
							  int		numleaf,		// Number of leaf cells on host
							  float		*h_bt)			// Boundary values from host

{
///	External variables are exposed 
	extern float*		d_bt;
	extern float*		d_maxgrad;
	extern float*		h_maxgrad;
	extern float*		d_res;
	extern float*		h_res;
	extern cudaStream_t stream1;
	extern cudaStream_t stream2;
/// Cool new externals
	extern float*	h_center;
	extern float*	d_center;
	extern float*	h_length;
	extern float*	d_length;
	extern float*	h_temp;
	extern float*	d_temp;
	extern float*	h_grad;
	extern float*	d_grad;
	extern int*		h_SFCng;
	extern int*		d_SFCng;

	h_center = (float*) malloc(2*numleaf * sizeof(float));
	h_length = (float*) malloc(  numleaf * sizeof(float));
	h_temp	 = (float*) malloc(  numleaf * sizeof(float));
	h_grad	 = (float*) malloc(  numleaf * sizeof(float));
	h_SFCng	 = (int*)   malloc(8*numleaf * sizeof(int)  );
	for (int i=0; i<numleaf; i++)
	{
		h_center[2*i]	= liste[i]->xcent;
		h_center[2*i+1]	= liste[i]->ycent;
		h_length[i]		= liste[i]->length;
		h_temp[i]		= liste[i]->temp;
		//h_grad[i]		= liste[i]->grad;
		for (int j=0; j<8; j++)
			h_SFCng[8*i+j]	= liste[i]->SFCng[j];
	}
	cudaMalloc ((void**)&d_center, 2*numleaf*sizeof(float));
	cudaMalloc ((void**)&d_length,   numleaf*sizeof(float));
	cudaMalloc ((void**)&d_temp,     numleaf*sizeof(float));
	cudaMalloc ((void**)&d_grad,     numleaf*sizeof(float));
	cudaMalloc ((void**)&d_SFCng,  8*numleaf*sizeof(int)  );
	cudaMemcpy (d_center, h_center, 2*numleaf*sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy (d_length, h_length,   numleaf*sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy (d_temp,   h_temp,     numleaf*sizeof(float), cudaMemcpyHostToDevice);
	//cudaMemcpy (d_grad,   h_grad,     numleaf*sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy (d_SFCng,  h_SFCng,  8*numleaf*sizeof(int)  , cudaMemcpyHostToDevice);

/// Streams for async memcopy of maximum residual from device to host
	cudaStreamCreate(&stream1);
	cudaStreamCreate(&stream2);

/// cell pointer list SFClist is converted to a d_cell list
/// boundary temperature is sent to device
	cudaMalloc ((void**)&d_bt, 4*sizeof(float));
	cudaMemcpy (d_bt, h_bt, 4*sizeof(float), cudaMemcpyHostToDevice);

/// memory allocated on device for residual, maximum gradient, host residual 
	cudaMalloc		((void**)&d_res, sizeof(float));
	cudaMallocHost	((void**)&h_res, sizeof(int));
	cudaMalloc		((void**)&d_maxgrad, sizeof(float));
	cudaMallocHost	((void**)&h_maxgrad, sizeof(int));
}


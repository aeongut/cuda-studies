#include	"cu_def.h"

extern "C" void cu_reinitialize(cell	**liste,	// SFClist on host
								int		numleaf,	// Number of cells on host
								cell	**d_list,	// *d_list is the address of the device data structure
								size_t	&pitch)		// pitch is the pitch of the device data structure
{
/// cell pointer list SFClist is converted to a cell list
	cell *h_list = (cell*) malloc(numleaf * sizeof(cell));
	for (int i=0; i<numleaf; i++)
		h_list[i] = *liste[i];

/// h_list is copied to device
	cudaFree(*d_list);
	cudaMallocPitch ((void**)d_list, &pitch, sizeof(cell), numleaf);
	cudaMemcpy2D (*d_list, pitch, h_list, sizeof(cell), sizeof(cell), numleaf, cudaMemcpyHostToDevice);

//	Eri�im �ekli:
//	cell *neu = (cell*)((char*)*d_list+0*pitch);
//	printf("%d\n",neu->level);
}


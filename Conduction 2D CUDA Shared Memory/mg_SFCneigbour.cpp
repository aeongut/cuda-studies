#include "meshgen.h"

void meshgen::mg_SFCneighbour()
{
	for (i=0; i<numleaf; i++)
		for (int j=0; j<4; j++)
			if (SFClist[i]->ng[j] != 0)
				if (SFClist[i]->ng[j]->ch[0] != 0)
				{
					SFClist[i]->SFCng[2*j]   = SFClist[i]->ng[j]->ch[(j+4+2)%4]->SFCorder;
					SFClist[i]->SFCng[2*j+1] = SFClist[i]->ng[j]->ch[(j+4+1)%4]->SFCorder;
				}
				else
				{
					SFClist[i]->SFCng[2*j]   = SFClist[i]->ng[j]->SFCorder;
					SFClist[i]->SFCng[2*j+1] = SFClist[i]->ng[j]->SFCorder;
				}
			else
			{
				SFClist[i]->SFCng[2*j]	 = -1;
				SFClist[i]->SFCng[2*j+1] = -1;
			}
}


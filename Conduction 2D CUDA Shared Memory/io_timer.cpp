#include "meshgen.h"

void meshgen::io_timer(int judge)
{
	switch (judge)
	{
		case 0:
			startTime = time(NULL);
			break;
		case 1:
			endTime = time(NULL);
			cout << "Elapsed time is " << difftime(endTime, startTime) << " seconds." << endl;
			break;
		default:
			cout << "Wrong argument for io_timer() function." << endl;
			break;
	}
}
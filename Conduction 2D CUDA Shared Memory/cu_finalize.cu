#include	"cu_def.h"

extern "C" void cu_finalize(kernelcell *d_list, float *d_bt, float *d_res, float *d_maxgrad, float *h_res, float *h_maxgrad)
{
	cudaFree(d_list);
	cudaFree(d_bt);
	cudaFree(d_res);
	cudaFree(d_maxgrad);
	cudaFreeHost(h_res);
	cudaFreeHost(h_maxgrad);
}


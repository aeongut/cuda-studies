#ifndef _CU_DEF_H_
#define _CU_DEF_H_

#include	<stdio.h>
#include	<stdlib.h>
#include	<math.h>
#include	"cuda.h"
#include	"datastruct.h"

#define threadsPerBlock 32		// Threads per block: same for all CUDA functions since they include this header.

struct kernelcell
{
	int		SFCng[8];			// neigbour information is kept as the loacations on SFC curve
	float	xy[2];				// coordinates of the center of a cell: kept in the order x and y
	float	length;				// side length of a cell
	float	temp;				// temperature of a cell
	float	grad;				// gradient of a cell
	struct	kernelcell *ng[8];	// using SFCng[8], these pointer are filled by kernels to easily reach neighbours
};

typedef struct cell cell;
typedef struct kernelcell kernelcell;

extern "C" void cu_initialize(cell**, int, float*, struct kernelcell**, size_t&, float**, float**, float**, float**, float**);
extern "C" void cu_reinitialize(cell**, int, cell**, size_t&);
extern "C" void cu_finalize(kernelcell*, float*, float*, float*, float*, float*);
extern "C" void cu_RKsolver(int, kernelcell*, size_t,  float*, float*);
extern "C" void cu_gradcalc(int, cell*, size_t,  float*, float*);
extern "C" void cu_fetch(cell**, int, kernelcell*, size_t);
extern "C" void cu_getres(float*, float*);
extern "C" void cu_getmaxgrad(float*, float*);

#endif

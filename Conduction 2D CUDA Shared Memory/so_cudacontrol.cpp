#include	"meshgen.h"
#include	"cu_def.h"

void meshgen::so_cudacontrol()
{
	int			counter=0;
	kernelcell	*d_list=NULL;
	size_t		pitch;
	float		inres, *d_bt=NULL, *d_res=NULL, *h_res=NULL, *d_maxgrad=NULL, *h_maxgrad=NULL;

	cu_initialize(SFClist, numleaf, bt, &d_list, pitch, &d_bt, &d_res, &h_res, &d_maxgrad, &h_maxgrad);

	do
	{
		cu_RKsolver(numleaf, d_list, pitch, d_bt, d_res);
		cu_getres(d_res, h_res);
		if(counter == 0)
			*h_res = 1.0f;
/*		if(counter == 1) 
			inres = *h_res;
		if(*h_res <= 0.1f * inres)
		{
			inres = *h_res;
			cu_gradcalc(numleaf, d_list, pitch, d_bt, d_maxgrad);
			cu_getmaxgrad(d_maxgrad, h_maxgrad);
			cu_fetch(SFClist, numleaf, d_list, pitch);
			so_findrefcell(root, h_maxgrad);
			so_refine(root);
			mg_SFCcontrol(root);
			cu_reinitialize(SFClist, numleaf, &d_list, pitch);

			cout << counter << '\t' <<"refinement was performed\n";
		}
*/		if(counter%50==0)
			cout << counter << '\t' << scientific << *h_res << endl;
		counter = counter+1;
	}
	while(*h_res>=1E-5);
	cout << counter << '\t' << scientific << *h_res << endl << "DONE!" << endl;
	cu_fetch(SFClist, numleaf, d_list, pitch);
	cu_finalize(d_list, d_bt, d_res, d_maxgrad, h_res, h_maxgrad);

}

//	Eri�im �ekli:
//	cell *neu = (cell*)((char*)d_list+0*pitch);
//	printf("%f\n",*d_res);
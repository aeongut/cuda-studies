#include	"cu_def.h"
#include	"RKsolver.kernel"

extern "C" void cu_RKsolver(int numleaf, kernelcell *d_list, size_t pitch,  float *d_bt, float *d_res)
{	
	cudaStream_t stream1;
	cudaStreamCreate(&stream1);

	cudaMemset(d_res, 0, sizeof(float));

	dim3 dimBlock(threadsPerBlock,1,1);
	dim3 dimGrid((numleaf+threadsPerBlock-1)/threadsPerBlock,1);


	RKsolver_kernel<<<dimGrid, dimBlock, 0, stream1>>>(numleaf, d_list, pitch, d_bt, d_res);
	//fprintf(stderr, "%s", cudaGetErrorString (cudaGetLastError ()));
}


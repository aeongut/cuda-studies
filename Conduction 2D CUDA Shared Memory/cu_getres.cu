#include	"cu_def.h"

extern "C" void cu_getres(float* d_res, float* h_res)
{	
	cudaStream_t stream2;
	cudaStreamCreate(&stream2);

	cudaMemcpyAsync(h_res, d_res, sizeof(float), cudaMemcpyDeviceToHost, stream2);
	//cudaMemcpy(h_res, d_res, sizeof(float), cudaMemcpyDeviceToHost);
}

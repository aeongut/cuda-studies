#include	"cu_def.h"

extern "C" void cu_fetch(cell		**liste,		// SFClist on host
						 int		numleaf,
						 kernelcell	*d_list,
						 size_t		pitch)
{
	kernelcell *h_list = (kernelcell*) malloc(numleaf * sizeof(kernelcell));
	cudaMemcpy2D (h_list, sizeof(kernelcell), d_list, pitch, sizeof(kernelcell), numleaf, cudaMemcpyDeviceToHost);

	for (int i=0; i<numleaf; i++)
	{
		liste[i]->temp		= h_list[i].temp;
		liste[i]->grad		= h_list[i].grad;
	}
}
#include	"cu_def.h"

extern "C" void cu_initialize(cell			**liste,		// SFClist on host
							  int			numleaf,		// Number of cells on host
							  float			*h_bt,			// Boundary values on host
							  kernelcell	**d_list,		// *d_list is the address of the device data structure
							  size_t		&pitch,			// pitch is the pitch of the device data structure
							  float			**d_bt,			// *d_bt is the address of the device boundary values
							  float			**d_res,		// *d_rs is the address of the device residual
							  float			**h_res,		// *d_inres is the address of the kept device residual
							  float			**d_maxgrad,	// *d_maxgrad is the address of the device maximum grad
							  float			**h_maxgrad)	// *h_maxgrad is the address of the host maximum grad
{
	/// Cell pointer list SFClist is converted to kernelcell h_list.
	kernelcell *h_list = (kernelcell*) malloc(numleaf * sizeof(kernelcell));
	for (int i=0; i<numleaf; i++)
	{
		h_list[i].xy[0]		= liste[i]->xcent;
		h_list[i].xy[1]		= liste[i]->ycent;
		h_list[i].length	= liste[i]->length;
		h_list[i].temp		= liste[i]->temp;
		h_list[i].grad		= liste[i]->grad;
		for (int j=0; j<8; j++)
		{
			h_list[i].SFCng[j]	= liste[i]->SFCng[j];
			h_list[i].ng[j]		= NULL;
		}
	}

/// alist is copied to device

	cudaMallocPitch ((void**)d_list, &pitch, sizeof(kernelcell), numleaf);
	cudaMemcpy2D (*d_list, pitch, h_list, sizeof(kernelcell), sizeof(kernelcell), numleaf, cudaMemcpyHostToDevice);

	//Eri�im �ekli:
//	cell *neu = (cell*)((char*)*d_list+0*pitch);
//	printf("%d\n",neu->level);

/// boundary temperature is sent to device
	cudaMalloc ((void**)d_bt, 4*sizeof(float));
	cudaMemcpy (*d_bt, h_bt, 4*sizeof(float), cudaMemcpyHostToDevice);

/// memory allocated on device for residual, maximum gradient, host residual 
	cudaMalloc ((void**)d_res, sizeof(float));
	cudaMalloc ((void**)d_maxgrad, sizeof(float));
	cudaMallocHost ((void**)h_res, sizeof(int));
	cudaMallocHost ((void**)h_maxgrad, sizeof(int));
	
}

